/** @format */

import {Color, Constants} from '@common'
import {Dimensions, StyleSheet} from 'react-native'

export default StyleSheet.create({
    body: {
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    askText: {
        fontWeight: '300',
        fontSize: 18,
        color: '#555',
        marginTop:40,
    },
    answerText: {
        fontSize: 42,
        fontWeight: '600',
        color: '#FF4455',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.15,
        shadowRadius: 4.84,
        elevation: 5,
    },
    lottieImg: {
        width: 300,
        height: 300,
    },
    getAnswerButtonOut: {
        borderRadius: 10,
        paddingTop: 15,
        paddingBottom: 15,
        paddingLeft: 40,
        paddingRight: 40,
        backgroundColor: '#FF4455',

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,

        marginBottom: 70,
        

    },
    getAnswerButton: {
        fontSize: 24,
        fontWeight:'300',
        color:'#fff'
    }

})
