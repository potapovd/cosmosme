import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

import Animated from "react-native-reanimated";
import {AnimatedHeader} from "@components";
import {Languages} from '@common';
import LinearGradient from 'react-native-linear-gradient'
import {Lottie} from "@expo";
import Shimmer from 'react-native-shimmer';
import css from './styles';

// const yesImg = require('@images/lottie/yep');
// const noImg = require('@images/lottie/nope');
const yepAndNope = require('@images/lottie/yepandnope');
const loadingImg = require('@images/lottie/loading_game');

const gameObj = {
    'askYes': yepAndNope,
    'askNo': yepAndNope
}

class GameYesNo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lottieSrc: loadingImg,
            gameAnswer: '',
            scrollY: new Animated.Value(100),
        }
    }


    componentDidMount() {
        this.animation.play();
    }

    resetAnimation = () => {
        this.animation.reset();
        this.animation.play();
    };

    startGame = () => {
        this.setState({
            lottieSrc: loadingImg,
            gameAnswer: 'askAnswerLoading'
        }, () => {
            this.resetAnimation()
        })
        const random = Math.round(Math.random())
        setTimeout(() => {

            this.setState({
                lottieSrc: Object.values(gameObj)[random]
            }, () => {
                this.resetAnimation()
                this.setState({gameAnswer: Object.keys(gameObj)[random]})
            })

        }, 1600)

    }


    render() {
        return (
            <View style={{flex: 1}}>
                <LinearGradient colors={['#eee', '#eee']} style={{flex: 1}}>
                    {/*<AnimatedHeader*/}
                    {/*    scrollY={this.state.scrollY}*/}
                    {/*    label={Languages.bot}*/}
                    {/*/>*/}
                    {/*<ImageBackground*/}
                    {/*    style={{*/}
                    {/*        flex: 1,*/}
                    {/*        width: '100%',*/}
                    {/*        height: '100%',*/}
                    {/*        backgroundColor: '#fff'*/}
                    {/*    }}*/}
                    {/*    //source={Images.ChatBackground}*/}
                    {/*    resizeMode="repeat"*/}
                    {/*    resizeMode='contain'*/}
                    {/*>*/}
                    <View style={css.body}>
                        <Text style={css.askText}>{Languages.askText}</Text>
                        <Shimmer opacity={0.55} duration={800}>
                            <Text style={css.answerText}>{Languages[this.state.gameAnswer]}</Text>
                        </Shimmer>
                        <Lottie
                            ref={animation => {
                                this.animation = animation;
                            }}
                            style={css.lottieImg}
                            loop={true}
                            source={this.state.lottieSrc}
                            //style={styles.lottie}
                            //source={noImg}
                        />
                        <TouchableOpacity
                            onPress={this.startGame}
                            style={css.getAnswerButtonOut}
                        >
                            <Text style={css.getAnswerButton}>{Languages.askButton}</Text>
                        </TouchableOpacity>

                    </View>
                </LinearGradient>
                {/*</ImageBackground>*/
                }
            </View>
        );
    }

}

export default GameYesNo;
