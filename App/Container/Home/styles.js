/** @format */

import {Color} from "@common";
import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  scrollView: {
    //backgroundColor: '#e00',
    paddingBottom: 70,
    paddingTop: 40,
    //backgroundColor: Color.homeLines,
  },
  body: {
    // marginTop:30,
    // paddingTop: 30,
    flex: 1,
    //paddingBottom: 50,
    //backgroundColor: Color.homeBackground,
  },
  navbar: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    //backgroundColor: '#e00',
    borderBottomColor: '#dedede',
    borderBottomWidth: 0,
    height: 40,
    justifyContent: 'center',
  },
})
