/** @format */

import {AnimatedHeader, HorizonList} from '@components'
import {AppConfig, Config, Images} from '@common'
import React, {PureComponent} from 'react'
import {RefreshControl, View} from 'react-native'

import Animated from "react-native-reanimated";
import Icons from '@navigation/Icons'
import LinearGradient from 'react-native-linear-gradient'
import PropTypes from 'prop-types'
import {connect} from 'react-redux';
import styles from './styles'

class Horizontal extends PureComponent {
    static propTypes = {
        fetchPostsByTag: PropTypes.func,
        onShowAll: PropTypes.func,
        onViewPost: PropTypes.func,
        goBack: PropTypes.func,
        deviceLanguage: PropTypes.string,
    }

    state = {scrollY: new Animated.Value(0), refreshing: false}

    onRefresh = () => {
        this.setState({refreshing: true})
        if (this.props.deviceLanguage == 'ru') {
            AppConfig.ru.HorizonLayout.map((config, index) => {
                this.props.fetchPostsByTag(1, config.tags, config.categories, index, this.props.deviceLanguage)
            })
        } else {
            AppConfig.en.HorizonLayout.map((config, index) => {
                this.props.fetchPostsByTag(1, config.tags, config.categories, index, this.props.deviceLanguage)
            })
        }
        setTimeout(() => {
            this.setState({refreshing: false})
        }, 2500)
    }

    render() {
        const {onShowAll, onViewPost, goBack} = this.props

        //let HorizonLayout
        const HorizonLayout = AppConfig[this.props.deviceLanguage].HorizonLayout.map((config, index) => (
                <HorizonList
                    horizontal
                    key={`hlist-${index}`}
                    config={config}
                    index={index}
                    onBack={() => goBack()}
                    onShowAll={onShowAll}
                    onViewPost={onViewPost}
                    deviceLanguage={this.props.deviceLanguage}
                />
            ))

        return (
            <View style={styles.body}>
                <LinearGradient colors={['#eee', '#eee']} style={{flex: 1}}>
                    {/* <AnimatedHeader
                        image={Images.logo}
                        right={Config.showLayoutButton && Icons.Layer()}
                        scrollY={this.state.scrollY}
                    /> */}

                    <Animated.ScrollView
                        contentContainerStyle={styles.scrollView}
                        scrollEventThrottle={1}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                            />
                        }
                        onScroll={Animated.event(
                            [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                            {useNativeDriver: true}
                        )}
                    >
                        {HorizonLayout}
                    </Animated.ScrollView>
                </LinearGradient>
            </View>
        )
    }
}

//export default Horizontal

const mapStateToProps = (state) => {
    const deviceLanguage = state.language.language.languageTag;
    return {deviceLanguage};
};
export default connect(mapStateToProps)(Horizontal);
