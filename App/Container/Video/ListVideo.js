/** @format */

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {FlatList, View, Button, Text, StyleSheet, TextInput} from 'react-native'
import Animated from "react-native-reanimated";
import LinearGradient from 'react-native-linear-gradient';

import {connect} from 'react-redux'
import {fetchVideos} from '@redux/actions'
import {Languages, Config} from '@common'
import {Spinkit, AnimatedHeader, FlatButton} from '@components'
import VideoControl from './VideoControl'

import css from './style'

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList)
//import DynamicTabView from "react-native-dynamic-tab-view";


class ListVideo extends Component {
    static propTypes = {
        fetchVideos: PropTypes.func,
        onViewPost: PropTypes.func,
        videos: PropTypes.array,
        navigation: PropTypes.object,
        isFetching: PropTypes.bool,
    }


    constructor(props) {
        super(props)
        this.page = 1
        this.isNextPost = false
        this.state = {
            scrollY: new Animated.Value(0),
            filter: '',
            //filter: 'video',
            //videosAll: this.props.videos,
            //videos: this.props.videos,
            //videos: []
        }
    }





    // _renderItem = (item, index) => {
    //     //console.log("renderItem ==>>>", index, foo);
    //     return (
    //         // <View
    //         //   key={item["key"]}
    //         //   style={{ backgroundColor: item["color"], flex: 1 }}
    //         // >
    //         // <Text>
    //         // {item.title}
    //         // </Text>
    //         // </View>
    //         <AnimatedFlatList
    //             //style={{flex:1}}
    //             style={{margin: 4}}
    //             numColumns={2}
    //             columnWrapperStyle={{flex: 1}}
    //             data={this.props.videos}
    //             onScroll={Animated.event(
    //                 [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
    //                 {useNativeDriver: true}
    //             )}
    //             scrollEventThrottle={1}
    //             contentContainerStyle={css.listView}
    //             keyExtractor={(item, index) => `video-${item.id || index}`}
    //             renderItem={this.renderItem(item.type)}
    //             ListFooterComponent={this.renderFooter}
    //             onEndReached={this.isNextPost && this.nextPosts}
    //         />
    //     );
    // };
    //
    // onChangeTab = index => {
    // };


    componentDidMount() {
        //console.log('deviceLanguage :'+this.props.deviceLanguage)
        if (this.page === 1) {
            //this.props.fetchVideos(this.page, null, Config.CategoryVideo)
            this.props.fetchVideos(this.page, this.props.deviceLanguage)
        }

        //console.log(this.props.videos);
        //const {videos} = this.props
        // const filteredData = this.state.filter
        //     ? videos.filter(x =>
        //         x.format.toLowerCase().includes(this.state.filter.toLowerCase())
        //     )
        //     : videos;
        //this.setState({videos})

    }


    // componentWillReceiveProps(nextProps) {
    //     console.log('12')
    //     const filteredData = this.state.filter
    //         ? nextProps.videos.filter(x =>
    //             x.format.toLowerCase().includes(this.state.filter.toLowerCase())
    //         )
    //         : nextProps.videos;
    //     //this.setState({videos})
    //     this.setState({
    //         videos: filteredData,
    //     });
    // }

    // componentDidUpdate(props, state) {
    //     console.log('1')
    //     const filteredData = state.filter
    //         ? props.videos.filter(x =>
    //             x.format.toLowerCase().includes(state.filter.toLowerCase())
    //         )
    //         : props.videos;
    //
    //     //if (state.prevCurNum !== props.initNum) {
    //     return {
    //         //videos: props.videos,
    //         videos: filteredData,
    //         //prevCurNum: props.initNum
    //     }
    // }
    //
    // static getDerivedStateFromProps(props, state) {
    //
    //     const filteredData = state.filter
    //         ? props.videos.filter(x =>
    //             x.format.toLowerCase().includes(state.filter.toLowerCase())
    //         )
    //         : props.videos;
    //
    //     //if (state.prevCurNum !== props.initNum) {
    //     return {
    //         //videos: props.videos,
    //         videos: filteredData,
    //         //prevCurNum: props.initNum
    //     }
    //     //} else {
    //     //    return {prevCurNum: props.initNum}
    //     // }
    // }


    shouldComponentUpdate(nextProps) {
        return nextProps.videos.length !== this.props.videos.length
    }

    onViewPost = (item, index) => {
        this.props.onViewPost(item, index, this.props.videos)
    }

    renderItem = ({item, index}) => {

        return (
            <View
                style={css.videoBlockOut}
            >
                <VideoControl
                    onViewPost={() => this.onViewPost(item, index, this.props.videos)}
                    video={item}
                    navigation={this.props.navigation}
                />
            </View>
        )
    }

    nextPosts = () => {
        this.page += 1
        this.isNextPost = true
        this.props.fetchVideos(this.page)
    }

    renderFooter = () => {
        const {isFetching} = this.props
        //if (isFetching) return <Spinkit />
        return (
            <View style={css.more}>
                <FlatButton
                    name="arrow-down"
                    text={isFetching ? Languages.loading : Languages.more}
                    load={this.nextPosts}
                />
            </View>
        )
    }


    // filterVideo = () => {
    //     console.log("this.state.filter ->" + this.state.filter)
    //     this.setState({filter: 'video'})
    //     const filteredData = this.state.filter
    //         ? this.state.videosAll.filter(x =>
    //             x.format.toLowerCase().includes(this.state.filter.toLowerCase())
    //         )
    //         : this.state.videosAll;
    //     this.setState({videos: filteredData})
    //     console.log("this.state.filter -->>>" + this.state.filter)
    // }
    // filterAudio = () => {
    //     console.log("this.state.filter ->" + this.state.filter)
    //     this.setState({filter: 'audio'})
    //     const filteredData = this.state.filter
    //         ? this.state.videosAll.filter(x =>
    //             x.format.toLowerCase().includes(this.state.filter.toLowerCase())
    //         )
    //         : this.state.videosAll;
    //     this.setState({videos: filteredData})
    //     console.log("this.state.filter -->>>" + this.state.filter)
    // }

    // onSearch = (e) => {
    //     let text = e.toLowerCase()
    //
    //
    //     this.setState({searchText: text});
    //
    //     let filteredData = this.state.videosAll.filter(function (item) {
    //         //return item.format.includes(text);
    //         return item.title.rendered.toLowerCase().includes(text.toLowerCase());
    //     });
    //
    //     this.setState({videos: filteredData});
    //
    //     // const filteredData = text
    //     //     ? this.state.videosAll.filter(x =>
    //     //         x.format.toLowerCase().includes(this.state.filter.toLowerCase())
    //     //     )
    //     //     : this.state.videosAll;
    //     // this.setState({videos: filteredData},()=>{
    //     //     console.log("this.state.filter -2")
    //     // })
    //
    //
    //     // let trucks = this.state.videos
    //     // let filteredName = trucks.filter((item) => {
    //     //     return item.format.toLowerCase().match(text)
    //     // })
    //     // if (!text || text === '') {
    //     //     console.log("this.state.filter 0")
    //     //     this.setState({
    //     //         videos: this.props.videos
    //     //     })
    //     // } else if (!Array.isArray(filteredName) && !filteredName.length) {
    //     //     console.log("this.state.filter 1")
    //     //     // set no data flag to true so as to render flatlist conditionally
    //     //     this.setState({
    //     //         noData: true
    //     //     })
    //     // } else if (Array.isArray(filteredName)) {
    //     //     console.log("this.state.filter 2" +filteredName)
    //     //     this.setState({
    //     //         noData: false,
    //     //         videos: filteredName
    //     //     },()=>{
    //     //         console.log(this.state.videos)
    //     //     })
    //     // }
    // }

    render() {
        const {videos} = this.props
        // //console.log("0 "+this.state.videos);
        // // console.log("this.state.filter " + this.state.filter)
        // const filteredData = this.state.filter
        //     ? videos.filter(x =>
        //         x.format.toLowerCase().includes(this.state.filter.toLowerCase())
        //     )
        //     : videos;
        // this.setState({media: videos}, () => {
        //     console.log("1 "+this.state.media);
        //     //this.forceUpdate();
        // })
        // console.log("2 "+this.state.media);

        // const filteredData = this.state.filter
        //     ? this.state.videosAll.filter(x =>
        //         x.format.toLowerCase().includes(this.state.filter.toLowerCase())
        //     )
        //     : this.state.videosAll;

        return (

            <View style={css.body}>
                <LinearGradient colors={['#ffd8e8', '#fac1d8']} style={{flex: 1}}>
                    <AnimatedHeader scrollY={this.state.scrollY} label={Languages.video}/>

                    {/*<Button>ALL</Button>*/}
                    {/*<Button>VIDEO</Button>*/}
                    {/*<Button>AUDIO</Button>*/}
                    {/*<View*/}
                    {/*    style={{marginTop: 40,backgroundColor:'#fff'}}*/}
                    {/*>*/}

                    {/* <TextInput*/}
                    {/*    //style={[styles.input]}*/}
                    {/*    //direction={Constants.RTL ? 'rtl' : ''}*/}
                    {/*    autoCapitalize="none"*/}
                    {/*    placeholder={`${Languages.search}...`}*/}
                    {/*    placeholderTextColor="#999"*/}
                    {/*    underlineColorAndroid="transparent"*/}
                    {/*    clearButtonMode="while-editing"*/}
                    {/*    //value={searchText}*/}
                    {/*    onChangeText={this.onSearch}*/}
                    {/*    //value={this.state.filter}*/}
                    {/*/>*/}

                    {/*<Button*/}
                    {/*    title={"audio"}*/}
                    {/*    onPress={this.filterAudio}*/}
                    {/*    //onPress={() => this.setState({filter: 'video'})}*/}
                    {/*/>*/}
                    {/*<Button*/}
                    {/*    title={"video"}*/}
                    {/*    onPress={this.filterVideo}*/}
                    {/*    //onPress={() => this.setState({filter: 'audio'})}*/}
                    {/*/>*/}


                    {/*</View>*/}


                    <AnimatedFlatList
                        //style={{flex:1}}
                        style={{margin: 4}}
                        numColumns={2}
                        columnWrapperStyle={{flex: 1}}
                        //data={this.state.videos}
                        //data={this.state.videos && this.state.videos.length > 0 ? this.state.videos : this.state.videosAll}
                        data={videos}
                        onScroll={Animated.event(
                            [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                            {useNativeDriver: true}
                        )}
                        scrollEventThrottle={1}
                        contentContainerStyle={css.listView}
                        keyExtractor={(item, index) => `video-${item.id || index}`}
                        renderItem={this.renderItem}
                        ListFooterComponent={this.renderFooter}
                        onEndReached={this.isNextPost && this.nextPosts}
                    />

                    {/*<DynamicTabView*/}
                    {/*    //style={{marginTop:0}}*/}
                    {/*    //data={this.data}*/}
                    {/*    data={videos}*/}
                    {/*    renderTab={this.renderItem}*/}
                    {/*    defaultIndex={this.state.defaultIndex}*/}
                    {/*    containerStyle={styles.container}*/}
                    {/*    headerBackgroundColor={'#b9e4e5'}*/}
                    {/*    headerTextStyle={styles.headerText}*/}
                    {/*    onChangeTab={this.onChangeTab}*/}
                    {/*    headerUnderlayColor={'gold'}*/}
                    {/*/>*/}

                </LinearGradient>
            </View>

        )
    }
}


// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         marginTop: 40,
//     },
//     // headerContainer: {
//     //     //marginTop: 16
//     //     backgroundColor: '#fff'
//     // },
//     headerText: {
//         color: 'black'
//     },
//     // tabItemContainer: {
//     //   backgroundColor: "#cf6bab"
//     // }
// });

const mapStateToProps = ({posts, language}) => {
    return {
        videos: posts.videos,
        isFetching: posts.isFetching,
        deviceLanguage: language.language.languageTag
    }
}
export default connect(mapStateToProps, {fetchVideos})(ListVideo)
