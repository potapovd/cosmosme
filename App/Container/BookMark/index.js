import {Events, Languages} from "@common";
import {FlatList, Image, ListView, RefreshControl, Text, View} from "react-native";
import React, {Component} from "react";

import Animated from "react-native-reanimated";
import {AnimatedHeader} from "@components";
import Icons from "@navigation/Icons";
import LinearGradient from 'react-native-linear-gradient';
import {PostReadLater} from "@components";
import User from "@services/User";
import {connect} from "react-redux";
import {fetchPostsBookmark} from "@redux/actions";
import styles from "./styles";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

class BookMark extends Component {
    constructor(props) {
        super(props);
    }

    state = {
        refreshing: false,
        scrollY: new Animated.Value(0),
    }

    UNSAFE_componentWillMount() {
        Events.onClearPosts(this.clearPosts.bind(this));
        this.props.fetchPostsBookmark();
    }


    componentDidMount() {
        this.props.fetchPostsBookmark();
        // this.props.navigation.addListener('focus', this._onFocus);
        // this.props.navigation.addListener('blur', this._onBlur);
    }

    componentWillUnmount() {
        this.props.fetchPostsBookmark();
        // this.props.navigation.removeListener('blur', this._onBlur);
        // this.props.navigation.removeListener('focus', this._onFocus);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.isFocused !== this.props.isFocused) {
            this.props.fetchPostsBookmark();
        }
    }


    clearPosts() {
        User.clearPosts(true);
        this.props.fetchPostsBookmark();
    }

    onViewPost(item, index, parentPosts) {
        this.props.onViewPost(item, index, parentPosts)
    }

    renderItem({item, index}) {
        return <PostReadLater 
        post={item}
        onViewPost={this.onViewPost.bind(this, item, index, this.props.bookmark.posts)}/>
    }

    onRefresh = () => {
        this.setState({refreshing: true},()=>{
            this.props.fetchPostsBookmark();
        })
        setTimeout(() => {
            this.setState({refreshing: false})
        }, 800)
    }

    render() {
        const {bookmark} = this.props;


        return (
            <View style={styles.body}>
                <LinearGradient colors={['#eee', '#eee']} style={{flex: 1}}>

                     {/* <AnimatedHeader
                        scrollY={this.state.scrollY}
                        label={Languages.textBookMark}
                    />  */}
                    {/*{bookmark.posts.length != 0 && <View style={styles.topBar}>{Icons.Clear()}</View>}*/}
                    {bookmark.posts.length == 0 &&
                      <View style={styles.emptyView}><Text style={[styles.empty]}>{Languages.noBookmark}</Text></View>
                    }

                    {/* <FlatList */}
                    <AnimatedFlatList
                        style={styles.flatlist}
                        contentContainerStyle={{paddingBottom:150}}
                        horizontal={false}
                        keyExtractor={(item, index) => item.id}
                        renderItem={this.renderItem.bind(this)}
                        // onScroll={Animated.event(
                        //     [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                        //     {useNativeDriver: true}
                        // )}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                            />
                        }
                        data={bookmark.posts}/>
                        
                </LinearGradient>
            </View>
        )
    }
}

const mapStateToProps = ({bookmark}) => ({bookmark});
module.exports = connect(mapStateToProps, {fetchPostsBookmark})(BookMark);
