/** @format */

import { Color, Constants } from '@common'
import React, {
  Dimensions,
  PixelRatio,
  Platform,
  StyleSheet,
} from 'react-native'

const { width, height, scale } = Dimensions.get('window'),
  vw = width / 100,
  vh = height / 100,
  vmin = Math.min(vw, vh),
  vmax = Math.max(vw, vh)

export default StyleSheet.create({
  body: {
    flex: 1,
  },
  flatlist: {
    //flex: 1,
    width: width,
    paddingTop: 40,
    //paddingBottom: 80
  },
  topBar: {
    width: width,
    height: 30,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 15,
  },
  empty: {
    marginTop: 16,
    marginRight: 16,
    marginBottom: 16,
    marginLeft: 16,
    fontWeight:'300',
    textAlign: 'center',
    color:'#777'
  },
  emptyView: {
    marginTop: 60,
    //alignItems: 'flex-start',
    textAlign: 'center'
  },
})
