/** @format */

import {StyleSheet, Platform, Dimensions} from 'react-native'
import {Constants, Color} from '@common'

const {width, height} = Dimensions.get('window')

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.searchBackground,
    },
    searchWrap: {
        margin: 20,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderRadius: 9,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        // ...Platform.select({
        //     ios: {
        //         borderRadius: 9,
        //         shadowColor: '#333',
        //         borderColor: '#fff',
        //         borderWidth: 0,
        //         shadowOffset: {
        //             width: 0,
        //             height: 4,
        //         },
        //         shadowRadius: 5,
        //         shadowOpacity: 0.25,
        //     },
        //     android: {
        //         elevation: 4,
        //     },
        // }),
    },
    searchIcon: {
        width: 15,
        height: 15,
        resizeMode: 'contain',
        tintColor: '#999',
        marginHorizontal: 10,
    },
    input: {
        flex: 1,
        color: '#333',
        fontSize: 16,
        textAlign: Constants.RTL ? "right" : 'left',
        zIndex: 9,
        borderRadius: 9,
        fontFamily: Constants.fontFamily,
        ...Platform.select({
            ios: {
                marginVertical: 15,
            },
            android: {
                //marginTop: 8,
            },
        }),
        marginRight: Constants.RTL ? 10 : 0
    },

    msg: {
        //fontSize: Constants.fontText.fontSizeMax,
        fontFamily: Constants.fontFamily,
        color: '#999',
        marginBottom: 100,
        textAlign: 'center',
        width: '100%',
        fontWeight: '300',
        fontSize: 18,
    },

    searchView: {
        zIndex: -7,
        flex: 1,
        //alignItems: 'flex-end',
        //justifyContent: 'flex-start',
        justifyContent: 'space-between'
    },
    emptyImage: {
        marginTop: 0,
        width: width,
        height: width,
        resizeMode: 'contain'
    },
})
