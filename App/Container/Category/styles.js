/** @format */

import {Color, Images} from '@common';
import {Dimensions, Platform, StyleSheet} from 'react-native'

import Constants from '@common/Constants'

const {width, height} = Dimensions.get('window')

let imageHeight, iconPosition;
if ((width >= 1100)) {
    imageHeight = 400;
    iconPosition = '32%'
} else if (width >= 615) {
    imageHeight = 200;
    iconPosition = '29%'
} else {
    imageHeight = 80;
    iconPosition = '10%'
}

export default StyleSheet.create({
    categoryWrapper: {
        //backgroundColor: Color.categoryBackground,
        flex: 1,
    },
    flatlist: {
        //marginTop: 20,
        flexDirection: 'row',
        flexWrap: 'wrap',
        flex: 1,
    },
    fill: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    body: {
        flex: 1,
        backgroundColor: '#fff',
        //paddingTop: 30,
    },
    box: {
        borderRadius: 6,
        backgroundColor: '#eee',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
    },
    content: {
        backgroundColor: 'transparent',
        width,
        height: height * 90 / 100,
        position: 'absolute',
        top: 0,
        left: 0,
    },
    boxName: {
        flex: 1,
        alignItems: 'center',
        position: 'relative',
    },
    boxNameText: {
        color: '#fff',
        fontSize: 20,
        //marginTop: 20,
        fontWeight: '600',
        fontFamily:
            Platform.OS !== 'android'
                ? Constants.fontHeader
                : Constants.fontHeaderAndroid,
        backgroundColor: 'transparent',
    },
    boxCountText: {
        color: '#fff',
        fontFamily:
            Platform.OS !== 'android'
                ? Constants.fontFamily
                : Constants.fontHeaderAndroid,
        fontWeight: '400',
        fontSize: 13,
        marginTop: 2,
        backgroundColor: 'transparent',
    },
    boxCategory: {
        height: width / 4,
        width: width / 2 - 10,
        //marginTop: 20,
        //marginLeft: 10,
        //borderRadius: 2,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
    },
    viewBox: {
        backgroundColor: '#2AB5B3',
        //borderRadius: 9,
        overflow: 'hidden',
    },
    imageBox: {
        //borderRadius: 6,
        resizeMode: 'cover',
    },
    image: {
        width: width - 30,
        height: imageHeight,
        //borderRadius: 9,
        overflow: 'hidden',

    },
    imageView: {
        //borderRadius: 6,
        width: width - 30,
        marginLeft: 15,
        height: imageHeight,
        marginBottom: 20,
        borderRadius: 10,
        overflow: 'hidden',
        //backgroundColor: 'rgba(255, 216, 232, .30)',
        //borderColor:'rgba(255, 255, 255, 0.0)',
        //borderWidth: 2,
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 3,
        // },
        // shadowOpacity: 0.29,
        // shadowRadius: 4.65,
        // elevation: 7,

        //borderRadius: 5,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.10,
        shadowRadius: 2.84,
        elevation: 5,


    },
    overlay: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255,255,255, 0.99)',
        flex: 1,
        position: 'absolute',
        top: 0,
        bottom: 0,
        width: width - 30,
        borderWidth: 1/2,
        borderColor: '#fff',
        //backgroundColor: "rgba(254, 224, 41, .33)",
        borderRadius: 20,
    },


    titleView: {
        width: width,
    },
    title: {
        fontSize: 24,
        textAlign: 'center',
        color: '#333',
        width: width - 40,
        // marginLeft: 20,
        // textShadowColor: 'rgba(0, 0, 0, 0.25)',
        // textShadowOffset: {width: -1, height: 1},
        //textShadowRadius: 10,
        fontFamily: Constants.fontHeader,
        fontWeight: '300'
    },
    logo: {
        position: 'absolute',

        top: iconPosition,
        left: 5,
        backgroundColor: 'transparent',
        width: 100,
        height: 100,
        resizeMode: 'contain',
        zIndex: 999,
        //opacity: .6

    },
    containerStyle: {

        //shadowColor: '#000',
        //borderRadius: 8,
        //backgroundColor: 'transparent',
        //backgroundColor:"red",
        //shadowOpacity: 0.1,
        //shadowRadius: 2,
        //shadowOffset: { width: 0, height: 6 },
        //shadowColor: '#333',
        //shadowRadius: 7,
        //shadowOpacity: 0.5,
        //elevation: 10,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.10,
        shadowRadius: 5.84,
        elevation: 5,


    },
    description: {
        fontSize: 14,
        color: '#777',
        marginTop: 4,
        backgroundColor: 'transparent',
        textAlign: 'center',
        width: width*0.5,
         marginLeft: 40,
        fontWeight:'300',
        fontFamily: Constants.fontFamily,
    },
    toolbarView: {
        height: 70,
        padding: 20,
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    toolbarText: {
        fontFamily: Constants.fontHeader,
        fontSize: 20,
        color: '#333',
    },
    toolbarIcon: {
        marginTop: 18,
        width: 30,
        height: 20,
        resizeMode: 'contain',
        tintColor: 'rgba(109, 103, 94, 1)',
    },
    scrollView: {
        paddingTop: 50,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingBottom: 60,
    },
})
