/** @format */

import {Analytics, AnimatedHeader, CategoryList, TouchableScale} from '@components';
import {Color, Config, Images, Languages} from '@common';
import {Image, ScrollView, Text, View} from 'react-native';
import React, {PureComponent} from 'react';
import {
    fetchCategories,
    setActiveCategory,
    setActiveLayout,
} from '@redux/actions';

import Animated from 'react-native-reanimated';
import FAB from '@custom/react-native-fab';
import Icon from '@expo/vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {flatten} from 'lodash';
import styles from './styles';

const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);

class CategoryHome extends PureComponent {
    static propTypes = {
        categories: PropTypes.array,
        fetchCategories: PropTypes.func,
        setActiveCategory: PropTypes.func,
        onViewPost: PropTypes.func,
        onViewCategory: PropTypes.func,
        setActiveLayout: PropTypes.func,
        selectedLayout: PropTypes.bool,
        deviceLanguage: PropTypes.string,
    };

    state = {
        scrollY: new Animated.Value(0),
    };

    componentDidMount() {
        this.props.fetchCategories(this.props.deviceLanguage);
    }

    fireAnalytics = async (category) => {
        await Analytics.onCategoryClick(category)
    }

    showCategory = (category) => {
         this.fireAnalytics(category);
         const {setActiveCategory, onViewCategory} = this.props;
         setActiveCategory(category.id);
         onViewCategory({config: {name: category.name, category: category.id}});
    };

    changeLayout = () => {
        this.props.setActiveLayout(!this.props.selectedLayout);
    };

    renderContent = () => {
        let {categories, onViewPost, selectedLayout} = this.props;
        categories.sort((a, b) => a.name < b.name);

        if (!selectedLayout) {
            return <CategoryList showBanner onViewPost={onViewPost}/>;
        }
        const backgroundColors = {
            // uncategorized_en: '#f2685d',
            // uncategorized_ru: '#f2685d',
            //
            // bioenergetika: '#ffd3ea',
            // bioenergy: '#ffd3ea',
            // blagosostoyanie: '#f2685d',
            // wealth: '#f2685d',
            // meditatsiya: '#c8ceff',
            // mindfulness_meditation: '#c8ceff',
            // ozdorovitelnaya_praktika: '#f4ab2a',
            // mind_body_spirit_practice: '#f4ab2a',
            // pozitivnaya_psihologiya: '#c8ceff',
            // positive_psychology: '#c8ceff',
            // lyubov_k_sebe: '#cccccc',
            // self_love: '#cccccc',
            // son: '#333',
            // meditation_for_sleep: '#333',
            // rasslablenie: '#556193',
            // relaxation: '#556193',


            // bioenergy: '#cccccc',
            // bioenergetika: '#cccccc',
            // mental_health: '#c8ceff',
            // mentalnoe_zdorove: '#c8ceff',
            // meditation: '#f4ab2a',
            // meditatsiya: '#f4ab2a',

            // chakra1: '#edebef',
            // chakra2: '#edebef',
            // chakra3: '#edebef',
            // chakra4: '#edebef',
            // chakra5: '#edebef',
            // chakra6: '#edebef',
            // chakra7: '#edebef',




            // "#f2685d",
            // "rgba(255, 212, 0, .12)","rgba(255, 255, 255, .92)", "rgba(236, 126, 173, .12)",
            // "rgba(202, 18, 25, .12)","rgba(168, 209, 226, .12)",
            //
            // "rgba(168, 207, 225, .37)","rgba(101, 72, 39, .37)","rgba(242, 129, 22, .37)",
            // "rgba(0, 111, 177, .37)","rgba(234, 125, 174, .37)","rgba(213, 225, 198, .37)",
            // "rgba(201, 19, 23, .37)","rgba(138, 88, 138, .37)","rgba(255, 213, 0, .37)",
            // "rgba(171, 201, 135, .37)",
            // "rgba(255, 213, 0, .37)","rgba(255, 128, 229, .37)","rgba(0, 111, 183, .37)",
        };

        return (
            <View style={{flex:1}}>
                <LinearGradient colors={['#eee', '#eee']} style={{flex:1}}>

                    {/* <AnimatedHeader
                        scrollY={this.state.scrollY}
                        label={Languages.category}
                    /> */}
                    <AnimatedScrollView
                        //style={{marginTop:20}}
                        scrollEventThrottle={1}
                        contentContainerStyle={styles.scrollView}
                        onScroll={Animated.event(
                            [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                            {useNativeDriver: true}
                        )}>
                        {typeof categories !== 'undefined' &&
                        categories.map((category, index) => {
                            if (category.parent == 0) {
                                // let imageCategory = Config.imageCategories[category.slug];
                                // if (imageCategory === undefined) {
                                //     imageCategory = Images.imageHolder;
                                // }
                                return (
                                    <View style={styles.containerStyle} key={'catehome-' + index}>
                                        <TouchableScale
                                            style={styles.imageView}
                                            key={index + 'img'}
                                            onPress={() => this.showCategory(category)}>
                                            {/*<Image style={styles.image} source={imageCategory}/>*/}
                                            {/* <Image
                                                //source={Images.Logo}
                                                //source={require('@images/category/cat1.png')}
                                                source={imageCategory}
                                                style={styles.logo}
                                                //blurRadius={1}
                                            /> */}
                                            <View
                                                style={[styles.overlay]}
                                                //style={[styles.overlay, {backgroundColor: backgroundColors[category.slug]}]}
                                                // style={[styles.overlay, {backgroundColor: backgroundColors[index]}]}
                                            >
                                                {/*<View style={[styles.overlay, { backgroundColor: backgroundColors[index]}]}>*/}
                                                {/*<View style={styles.overlay}>*/}
                                                <Text style={styles.title}>{category.name.toString().substring(0)}</Text>
                                                {category.description.length > 0 && (
                                                    <Text numberOfLines={2} style={styles.description}>
                                                        {category.description}
                                                    </Text>
                                                )}
                                            </View>
                                        </TouchableScale>
                                    </View>
                                );
                            }
                        })}
                    </AnimatedScrollView>

                </LinearGradient>

            </View>
        );
    };

    render() {
        return (
            <View style={styles.categoryWrapper}>
                {this.renderContent()}

                {Config.showSwitchCategory && (
                    <FAB
                        buttonColor={Color.toolbarTint}
                        iconTextColor="#FFFFFF"
                        onClickAction={this.changeLayout}
                        visible
                        iconTextComponent={<Icon name="exchange"/>}
                    />
                )}
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const selectedCategory = state.categories.selectedCategory;
    const categories = flatten(state.categories.list);
    const selectedLayout = state.categories.selectedLayout;
    const deviceLanguage = state.language.language.languageTag;
    return {categories, selectedCategory, selectedLayout, deviceLanguage};
};
export default connect(
    mapStateToProps,
    {
        fetchCategories,
        setActiveLayout,
        setActiveCategory,
    }
)(CategoryHome);
