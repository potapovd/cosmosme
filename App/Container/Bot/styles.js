/** @format */

import {Color, Constants} from '@common'
import {Dimensions, StyleSheet} from 'react-native'

export default StyleSheet.create({
    body: {
        flex: 1,
    },
    botchat: {
        flex: 1,
        paddingTop: 50,
    },
    typingText: {
        marginLeft: 10,
        marginBottom: 5,
        color: '#777',
    },
    leftWrapperStyle: {
        borderBottomWidth: 5,
        borderRightWidth: 5,
        borderBottomColor: Color.leftChatBorder,
        borderRightColor: Color.leftChatBorder,
        backgroundColor: Color.leftChat,
        borderRadius: 20,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        padding: 5,
    },
    leftTextStyle: {
        color: Color.leftChatText
    },
    rightWrapperStyle: {
        borderBottomWidth: 5,
        borderRightWidth: 5,
        borderBottomColor: Color.rightChatBorder,
        borderRightColor: Color.rightChatBorder,
        backgroundColor: Color.rightChat,
        borderRadius: 20,
        borderBottomRightRadius: 10,
        marginBottom: 10,
        padding: 5,
        right: 15,
        justifyContent: 'flex-end',
        marginRight: 0,
    },
    rightTextStyle: {
        color: Color.rightChatText
    },

    sendContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
    },
    sendInput: {
        //backgroundColor: 'red',
        //borderColor: '#cccccc',
        padding: 0,
        flex: 1,
        color: '#333',
        fontSize: 16,
    },
    sendButton: {
        width: 20,
        height: 20,
    }

})
