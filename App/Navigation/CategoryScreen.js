/** @format */

import React, { Component } from 'react'
import { Style, Config } from '@common'
import {Text} from 'react-native'
import { Category, NewCategory } from '@container'
import Icons from './Icons'

export default class CategoryScreen extends Component {
  static navigationOptions = {
    headerLeft: Icons.Home(),
    headerShown: false,
    headerStyle: Style.toolbar,
  }

  render() {
    const { navigate } = this.props.navigation
    if (Config.showSubCategoryScreen) {
      return (
        <NewCategory
          onViewPost={(item, index, parentPosts) =>
            navigate('postDetail', { post: item, index, parentPosts, backToRoute: 'category' })
          }
          onViewCategory={(config) => navigate('PostListScreen', config)}
        />
      )
    } else {
      return (
          <Category
              onViewPost={(item, index, parentPosts) =>
                  navigate('postDetail', { post: item, index, parentPosts, backToRoute: 'category' })
              }
              onViewCategory={(config) => navigate('PostListScreen', config)}
          />
      )
    }
  }
}
