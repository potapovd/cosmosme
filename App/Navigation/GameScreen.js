/** @format */

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Languages} from '@common'
import {Game} from '@container'

class GameScreen extends Component {
    static navigationOptions = {
        tabBarLabel: Languages.textBookMark,
        headerShown: false,
    }

    static propTypes = {
        navigation: PropTypes.object,
    }

    render = () => {
        const {navigate} = this.props.navigation
        return (
            <Game/>
            // <Search
            //     onViewPost={(post, index) =>
            //         navigate('searchPostDetail', { post, index, fromSearch: true })
            //     }
            // />
        )
    }
}

export default GameScreen
