/** @format */

import {Color, Images} from '@common'
import {TabBar, TabBarIcon} from '@components'

//import PostNewsScreen from './PostNewsScreen'
//import PostNewsContentScreen from './PostNewsContentScreen'
import BotScreen from './BotScreen'
import CategoryScreen from './CategoryScreen'
import CustomPageScreen from './CustomPageScreen'
import GameScreen from './GameScreen'
import HomeScreen from './HomeScreen'
import HorizontalScreen from './HorizontalScreen'
import PostDetailScreen from './PostDetailScreen'
import PostListScreen from './PostListScreen'
import React from 'react'
import ReadLaterScreen from './ReadLaterScreen'
import SearchScreen from './SearchScreen'
import SettingScreen from './SettingScreen'
//import PhotoScreen from './PhotoScreen'
import VideoScreen from './VideoScreen'
import {createAppContainer} from 'react-navigation'
import {createBottomTabNavigator} from 'react-navigation-tabs'
import {createStackNavigator} from 'react-navigation-stack'

const categoryStack = createStackNavigator(
    {
        category: {screen: CategoryScreen},
        PostListScreen: {screen: PostListScreen},
    },
    {
        // navigationOptions: {
        //     tabBarIcon: ({tintColor}) => (
        //         <TabBarIcon icon={Images.icons.category} tintColor={tintColor}/>
        //     ),
        //     headerTintColor: '#333',
        // },
    }
)

const newsStack = createStackNavigator(
    {
        home: {screen: HomeScreen},
        PostListScreen: {screen: PostListScreen},
        HorizontalScreen: {screen: HorizontalScreen},
    },
    {
        //headerShown: false,
        title: 'Home',
        tabBarLabel: "Home",
        // navigationOptions: {
        //     title: 'Home',
        //     tabBarLabel: "Home",
        //     tabBarIcon: ({tintColor}) => (
        //         <TabBarIcon icon={Images.icons.news} tintColor={tintColor}/>
        //     ),
        // },
    }
)

const videoStack = createStackNavigator(
    {
        video: {screen: VideoScreen},
    },
    {
        // navigationOptions: {
        //     tabBarIcon: ({tintColor}) => (
        //         <TabBarIcon icon={Images.icons.video} tintColor={tintColor}/>
        //     ),
        // },
    }
)

const searchStack = createStackNavigator(
    {
        search: {screen: SearchScreen},
        searchPostDetail: {screen: PostDetailScreen},
    },
    {
        headerShown: false,
        // navigationOptions: {
        //     tabBarIcon: ({tintColor}) => (
        //         <TabBarIcon icon={Images.icons.video} tintColor={tintColor}/>
        //     ),
        // },

    }
)

const botStack = createStackNavigator(
    {
        bot: {screen: BotScreen},
        //searchPostDetail: {screen: PostDetailScreen},
    },
    {
        headerShown: false,
        // navigationOptions: {
        //     tabBarIcon: ({tintColor}) => (
        //         <TabBarIcon icon={Images.icons.video} tintColor={tintColor}/>
        //     ),
        // },

    }
)

const gameStack = createStackNavigator(
    {
        game: {screen: GameScreen},
        //searchPostDetail: {screen: PostDetailScreen},
    },
    {
        headerShown: false,
        // navigationOptions: {
        //     tabBarIcon: ({tintColor}) => (
        //         <TabBarIcon icon={Images.icons.video} tintColor={tintColor}/>
        //     ),
        // },

    }
)

const readLaterStack = createStackNavigator(
    {
        readLater: {screen: ReadLaterScreen},
        //searchPostDetail: {screen: PostDetailScreen},
    },
    {
        headerShown: false,
        navigationOptions: {
            tabBarIcon: ({tintColor}) => (
                <TabBarIcon icon={Images.icons.video} tintColor={tintColor}/>
            ),
        },

    }
)

const AppNavigator = createBottomTabNavigator(
    {
        // bot: {
        //     screen: botStack,
        //     navigationOptions: {
        //         headerShown: false,
        //         tabBarIcon: ({tintColor}) => (
        //             <TabBarIcon icon={Images.icons.botIcon} tintColor={tintColor}/>
        //         ),
        //     },
        // },

        home: {
            screen: newsStack,
            navigationOptions: {
                //headerShown: false,
                tabBarIcon: ({tintColor}) => (
                    <TabBarIcon icon={Images.icons.homeIcon} tintColor={tintColor}/>
                ),
                //tabBarLabel: "Home",
                //title: 'Home',
                tabBarLabel: ({focused}) => {
                    //return <Text style={{fontSize: 14, fontWeight: '600', color: colors.primary}}>{focused ? route.name : ""}</Text>
                    return <Text>Foo</Text>
                },
            },
        },

        category: {
            screen: categoryStack,
            navigationOptions: {
                headerShown: false,
                tabBarIcon: ({tintColor}) => (
                    <TabBarIcon icon={Images.icons.categoryIcon} tintColor={tintColor}/>
                ),
            },
        },

        game: {
            screen: gameStack,
            navigationOptions: {
                headerShown: false,
                tabBarIcon: ({tintColor}) => (
                    <TabBarIcon icon={Images.icons.gameIcon} tintColor={tintColor}/>
                ),
            },
        },

        // search: {
        //     screen: searchStack,
        //     navigationOptions: {
        //         tabBarIcon: ({tintColor}) => (
        //             <TabBarIcon icon={Images.icons.searchIcon} tintColor={tintColor}/>
        //         ),
        //     },
        // },

        // video: {
        //     screen: videoStack,
        //     navigationOptions: {
        //         headerShown: false,
        //         tabBarIcon: ({tintColor}) => (
        //             <TabBarIcon icon={Images.icons.videoIcon} tintColor={tintColor}/>
        //         ),
        //     },
        // },


        // photo: {
        //     screen: PhotoScreen,
        //     navigationOptions: {
        //         tabBarIcon: ({tintColor}) => (
        //             <TabBarIcon icon={Images.icons.photoIcon} tintColor={tintColor}/>
        //         ),
        //     },
        // },
        readlater: {
            //screen: ReadLaterScreen,
            screen: readLaterStack,
            navigationOptions: {
                headerShown: false,
                tabBarIcon: ({tintColor}) => (
                    <TabBarIcon icon={Images.icons.favIcon} tintColor={tintColor}/>
                ),
            },
        },

        postDetail: {screen: PostDetailScreen},
        //customPage: {screen: CustomPageScreen},
        //setting: {screen: SettingScreen},
        //postNews: {screen: PostNewsScreen},
        //postNewsContent: {screen: PostNewsContentScreen},
    },
    {
        tabBarComponent: TabBar,
        tabBarPosition: 'bottom',
        swipeEnabled: false,
        animationEnabled: false,
        //initialRouteName: 'bot',
        labeled: true,
        tabBarOptions: {
            showIcon: true,
            showLabel: true,
            activeTintColor: Color.tabbarTint,
            inactiveTintColor: Color.tabbarColor,

 
        },
        lazy: true
    }
)

export default createAppContainer(AppNavigator)
