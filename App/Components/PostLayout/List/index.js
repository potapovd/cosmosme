/** @format */

import { CommentIcons, ImageCache } from '@components';
import { Constants, Tools, warn } from '@common';
import { Text, TouchableOpacity, View } from 'react-native';

import Icon from "@expo/vector-icons/SimpleLineIcons";
import PropTypes from 'prop-types';
import React from 'react';
import TimeAgo from 'react-native-timeago';
import css from './styles';

let moment = require('moment');
require('moment/locale/ru');

const List = ({ viewPost, category, post, deviceLanguage }) => {
  const imageURL = Tools.getImage(post, Constants.PostImage.small);
  moment.locale(deviceLanguage);

  const date = post.date;
  const title =
    typeof post.title !== 'undefined'
      ? Tools.formatText(post.title.rendered, 300)
      : '';
  const description =
    typeof post.excerpt !== 'undefined'
      ? Tools.formatText(post.excerpt.rendered, 300)
      : '';

  return (
    <TouchableOpacity onPress={viewPost} style={css.panelList}>
      <TouchableOpacity activeOpacity={0.9} onPress={viewPost}>
        <View style={css.imageListOut}>
          <ImageCache source={{ uri: imageURL }} style={css.imageList} />
        </View>
        <CommentIcons
          post={post}
          size={16}
          style={[css.heart, { right: Constants.RTL ? 10 : 0, top: 10 }]}
          hideShareIcon
          hideOpenIcon
          hideCommentIcon
        />
        {post.format == 'audio' && (
            <View
                style={[
                    css.iconVideo,
                    //width && {left: width / 2 - 15},
                    //height && {top: height / 3 - 10},
                ]}>
                <Icon name="music-tone-alt" size={20} style={css.iconPlay}/>
            </View>
        )}
      </TouchableOpacity>

      <View style={css.titleList} onPress={viewPost}>
        <TouchableOpacity activeOpacity={0.9} onPress={viewPost}>
        {/* <Text numberOfLines={1} style={css.nameList}> */}
        <Text numberOfLines={3} style={css.nameList}>
            {title}
          </Text>
        </TouchableOpacity>

        {description !== '' && (
          <Text
            onPress={viewPost}
            numberOfLines={2}
            style={css.descriptionList}>
            {description}
          </Text>
        )}

        <View style={{ flexDirection: 'row' }}>
          {/* <TimeAgo style={css.timeList} time={date} /> */}

          {category && (
            <TouchableOpacity>
              <Text style={css.category}>- {category}</Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    </TouchableOpacity>
  );
};

List.propTypes = {
  viewPost: PropTypes.func,
  category: PropTypes.any,
  post: PropTypes.object,
  deviceLanguage: PropTypes.string,
};


export default List;
