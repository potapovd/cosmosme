/** @format */

import {Dimensions, StyleSheet} from 'react-native'

import Color from '@common/Color'
import {Constants} from '@common'

const {width} = Dimensions.get('window')
const vw = width / 100

export default StyleSheet.create({
    panel: {
        //backgroundColor: '#f6ebeb',
        //borderColor: '#c1e9e7',
        //borderBottomWidth: 0.5,
        flexDirection: 'row',

        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 2,
        // },
        // shadowOpacity: 0.35,
        // shadowRadius: 3.84,
        // elevation: 5,
    },
    content: {
        width: vw * 63,
        marginLeft: vw * 2,
    },
    title: {
        fontSize: 16,
        marginLeft: 4,
        marginTop: 12,
        marginRight: 8,
        color: '#333',
        fontWeight: '400',
        textAlign: 'left'
    },
    description: {
        fontSize: 12,
        marginLeft: 4,
        marginTop: 10,
        marginRight: 8,
        color: '#333',
        fontWeight: '300',
        textAlign: 'left',
        ...Platform.select({
            android: {
                //paddingTop: 0
                fontFamily: Constants.fontFamily
            },
        }),

    },

    time: {
        color: '#999',
        fontSize: 10,
        marginBottom: 10,
        marginLeft: 4,
        marginTop: 6,
        backgroundColor: 'transparent',
    },

    category: {
        fontSize: 11,
        marginTop: 6,
        color: '#999',

    },

    image: {
        marginLeft: vw * 2,
        marginRight: vw * 2,
        marginTop: 12,
        marginBottom: 8,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        width: vw * 31,
        height: vw * 25,
        resizeMode: 'cover',
        borderRadius: 6,


    },
    heart: {
        position: 'absolute',
        zIndex: 9999,
        top: 1,
        right: 0,
    },

    panelList: {
        //backgroundColor: 'rgba(254, 246, 255, 0.33)',
        // backgroundColor: '#f6ebeb',
         borderColor: Color.homeLines,
         borderBottomWidth: 0.5,
        //backgroundColor:'transparent',
        flexDirection: 'row',

        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 2,
        // },
        // shadowOpacity: 0.35,
        // shadowRadius: 3.84,
        // elevation: 5,
    },
    imageListOut:{
        // backgroundColor:'red',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.35,
        shadowRadius: 3.84,
        elevation: 5,
    },
    imageList: {
        marginTop: 12,
        marginLeft: 8,
        marginRight: 8,
        marginBottom: 8,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        width: vw * 30,
        height: vw * 30 - 20,
        resizeMode: 'cover',
        borderRadius: 6,

    },
    titleList: {
        width: vw * 65,
        marginTop: 4,
    },
    nameList: {
        fontSize: 16,
        marginLeft: 2,
        marginTop: 6,
        marginRight: 8,
        color: '#333',
        fontWeight: '300',
        textAlign: 'left'
    },
    descriptionList: {
        fontSize: 12,
        marginLeft: 4,
        marginTop: 10,
        marginRight: 8,
        color: '#555',
        fontWeight: '300',
        textAlign: 'left',
        ...Platform.select({
            android: {
                //paddingTop: 0
                fontFamily: Constants.fontFamily
            },
        }),
    },
    timeList: {
        color: '#999',
        fontSize: 11,
        marginLeft: 4,
        marginTop: 10,
        backgroundColor: 'transparent',
    },
    iconPlay: {
        color: Color.playButtonArrow,
        backgroundColor: 'transparent',
        marginTop: 10,
        marginRight: 18,
        marginBottom: 10,
        marginLeft: 22,
        zIndex: 9999,
        width: 24,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.35,
        shadowRadius: 3.84,
        elevation: 5,
    },
    iconVideo: {
        alignItems: 'center',
        justifyContent: 'center',
        top: 35,
        left: 37,
        zIndex: 999,
        width: 46,
        height: 46,
        position: 'absolute',
        backgroundColor: Color.playButton,
        borderRadius: 36,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.35,
        shadowRadius: 3.84,
        elevation: 5,
    },
})
