/** @format */

import { CommentIcons, ImageCache } from '@components'
import { Constants, Tools } from '@common'
import { Image, Text, TouchableOpacity, View } from 'react-native'

import Icon from "@expo/vector-icons/SimpleLineIcons";
import PropTypes from 'prop-types'
import React from 'react'
import TimeAgo from 'react-native-timeago'
import css from './styles'

let moment = require('moment');
require('moment/locale/ru');

const ListRight = ({ viewPost, category, post, deviceLanguage }) => {
  const imageURL = Tools.getImage(post, Constants.PostImage.medium)
  const date = post.date
  const title = Tools.formatText(post.title.rendered, 100)
  const description = Tools.formatText(post.excerpt.rendered, 300)

  moment.locale(deviceLanguage);

  return (
     <TouchableOpacity onPress={viewPost} style={css.panel} >
      <View style={css.content}>
        <TouchableOpacity activeOpacity={0.9} onPress={viewPost}>
          <Text numberOfLines={2} style={css.title}>
            {title}
          </Text>
        </TouchableOpacity>

        {description !== '' && (
          <Text numberOfLines={3} style={css.description}>
            {description}
          </Text>
        )}

        <View style={{ flexDirection: 'row' }}>
          {/* <TimeAgo style={css.time} time={date}  /> */}

          {/*{category !== '' && (*/}
          {/*  <TouchableOpacity>*/}
          {/*    <Text style={css.category}>- {category}</Text>*/}
          {/*  </TouchableOpacity>*/}
          {/*)}*/}
        </View>
      </View>

      <TouchableOpacity activeOpacity={0.9} onPress={viewPost}>
         <View style={css.imageListOut}>
          <ImageCache source={{ uri: imageURL }} style={css.image} />
        </View>
        {post.format == 'audio' && (
            <View
                style={[
                    css.iconVideo,
                    //width && {left: width / 2 - 15},
                    //height && {top: height / 3 - 10},
                ]}>
                <Icon name="music-tone-alt" size={20} style={css.iconPlay}/>
            </View>
        )}

      </TouchableOpacity>


      <CommentIcons
        post={post}
        size={16}
        style={[css.heart, { right: 0, top: 10 }]}
        hideShareIcon
        hideOpenIcon
        hideCommentIcon
      />
    </TouchableOpacity>
  )
}

ListRight.propTypes = {
  viewPost: PropTypes.func,
  category: PropTypes.any,
  post: PropTypes.object,
}

export default ListRight
