/** @format */

import {CommentIcons, ImageCache} from "@components";
import {Constants, Tools} from "@common";
import {Text, TouchableOpacity, View} from "react-native";

import Icon from "@expo/vector-icons/SimpleLineIcons";
import PropTypes from "prop-types";
import React from "react";
import TimeAgo from "react-native-timeago";
import css from "./styles";

let moment = require('moment');
require('moment/locale/ru');



const ColumnLayout = ({viewPost, post, width, height, deviceLanguage}) => {
    const title =
        typeof post !== "undefined" ? Tools.formatText(post.title.rendered) : "";
    const imageURL = Tools.getImage(post, Constants.PostImage.medium);

    const date = typeof post.date === "undefined" ? "" : post.date;
    
    let videoUrl = "";
    if (typeof post !== "undefined") {
        videoUrl = post.content ? Tools.getLinkVideo(post.content.rendered) : "";
    }

    moment.locale(deviceLanguage);

    return (
        <TouchableOpacity
            activeOpacity={0.9}
            //style={[css.panel, width && {width: width + 15}]}
            style={[css.panel]}
            onPress={viewPost}>
            <View style={css.imageListOut}>
                <ImageCache
                    source={{uri: imageURL}}
                    //style={[css.image, width && {width}, height && {height}]}
                    style={[css.image, width && {width}, height && {height}]}
                />
            </View>

            {videoUrl.length > 0 && (
                <View
                    onPress={viewPost}
                    style={[
                        css.iconVideo,
                        width && {left: width / 2 - 15},
                        height && {top: height / 3 - 10},
                    ]}>
                    <Icon name="control-play" size={25} style={css.iconPlay}/>
                </View>
            )}
            
            {post.format == 'audio' && (
                <View
                    onPress={viewPost}
                    style={[
                        css.iconVideo,
                        width && {left: width / 2 - 15},
                        height && {top: height / 3 - 10},
                    ]}>
                    <Icon name="music-tone-alt" size={22} style={css.iconPlay}/>
                </View>
            )}


            <Text
                onPress={viewPost}
                numberOfLines={2}
                style={[css.name, width && {width: width - 4}]}>
                {title}
            </Text>

            {/* <Text onPress={viewPost} style={css.time}>
                <TimeAgo time={date}/>
            </Text> */}

            <CommentIcons
              post={post}
              size={16}
              style={css.heart}
              activeBackground="rgba(255, 255, 255, .3)"
              hideShareIcon
              hideOpenIcon
              hideCommentIcon
            />
        </TouchableOpacity>
    );
};

ColumnLayout.propTypes = {
    viewPost: PropTypes.func,
    width: PropTypes.number,
    height: PropTypes.number,
    post: PropTypes.object,
};

export default ColumnLayout;
