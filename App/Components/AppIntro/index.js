/** @format */

import {
    Animated,
    Dimensions,
    SafeAreaView,
    StatusBar,
    Text,
    TouchableOpacity,
    View,
} from "react-native";
import {Config, Languages} from "@common";
import {ContentContainer, Header} from "react-native-onboarding-component";
import React, {PureComponent} from "react";

import LinearGradient from 'react-native-linear-gradient'
import {Lottie} from "@expo";
import {connect} from "react-redux";
import {finishIntro} from "@redux/actions";
import styles from "./styles";

const {width: deviceWidth} = Dimensions.get("window");

const pages = Config.intro;

class AppIntro extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            currentIndex: 0,
        };
        this.scrollX = new Animated.Value(0);
        this.animations = new Map();
    }

    componentDidMount() {
        this.animations.get(this.state.currentIndex).play();
    }

    onScroll = (event) => {
        const {contentOffset} = event.nativeEvent;
        const currentIndex = Math.round(contentOffset.x / deviceWidth);

        if (this.state.currentIndex !== currentIndex) {
            this.animations.forEach((animation) => {
                animation.reset();
            });
            this.animations.get(currentIndex).play();
            this.setState({currentIndex});
        }
    };

    scrollTo = (index) => {
        console.log("index, deviceWidth scrollView: " + index, deviceWidth, this.scrollView)
        this.scrollView.scrollTo({
            x: deviceWidth * index,
            animated: true,
        });
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <LinearGradient colors={['#eee', '#eee']} style={{flex: 1}}>
                    {/*<StatusBar backgroundColor="blue" barStyle="light-content" />*/}

                    {/* <GradientBackgrounds
          colors={pages.map((page) => page.backgroundColor)}
          scrollX={this.scrollX}
          style={styles.gradientBackground}
        /> */}

                    <Animated.ScrollView
                        horizontal
                        ref={(scrollView) => {
                            this.scrollView = scrollView;
                        }}
                        showsHorizontalScrollIndicator={false}
                        pagingEnabled
                        scrollEventThrottle={1}
                        onScroll={Animated.event(
                            [{nativeEvent: {contentOffset: {x: this.scrollX}}}],
                            {useNativeDriver: true, listener: this.onScroll}
                        )}
                    >
                        {pages.map((page, index) => (
                            <View
                                key={`pages-${index}`}
                                style={[
                                    styles.card,
                                    {width: deviceWidth, flexDirection: "column"},
                                ]}
                            >
                                <Header style={{backgroundColor: "transparent"}}>
                                    <Lottie
                                        ref={(animation) => {
                                            if (animation) {
                                                this.animations.set(index, animation);
                                            }
                                        }}
                                        loop={false}
                                        style={styles.lottie}
                                        source={page.source}
                                    />
                                </Header>

                                <ContentContainer style={styles.bottomView}>
                                    <Text style={styles.title}>{page.title}</Text>
                                    <Text style={styles.description}>{page.description}</Text>
                                </ContentContainer>
                            </View>
                        ))}
                    </Animated.ScrollView>

                    <View style={styles.row}>
                        {/* <View style={styles.indicatorWrap}>*/}
                        {/*  <Indicator items={pages} scrollX={this.scrollX} />*/}
                        {/*</View> */}

                        <TouchableOpacity onPress={() => this.props.finishIntro()}>
                            <Text style={styles.btnText}>{Languages.introSkip}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => {
                                const currentIndex = Number(this.state.currentIndex);
                                if (Number(currentIndex + 1) === pages.length) {
                                    this.props.finishIntro();
                                } else {
                                    const next = Number(currentIndex + 1);
                                    this.scrollTo(next);
                                }
                            }}>
                            <Text style={styles.btnText}>
                                {pages[this.state.currentIndex].button}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </LinearGradient>
            </SafeAreaView>
        );
    }
}

export default connect(null, {finishIntro})(AppIntro);
