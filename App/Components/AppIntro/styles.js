import {Dimensions, Platform, StyleSheet} from 'react-native'

import {Color} from '@common'

const {width} = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.introSafeAreaBackground,
    },
    scrollView: {
        flex: 1,
        backgroundColor: Color.introBodyBackground,
    },
    card: {
        backgroundColor: 'transparent'
    },
    lottie: {
        width: '80%',
        maxWidth: 400,
        height: '100%',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.10,
        shadowRadius: 5.84,
        elevation: 5,
    },
    title: {
        fontSize: 32,
        fontWeight: '300',
        marginBottom: 20,
        color: '#333',
        textAlign: 'center'
    },
    description: {
        fontWeight: '300',
        color: '#333',
        textAlign: 'center',
        fontSize: 16,
        lineHeight: 28
    },
    row: {
        flexDirection: 'row',
        borderTopWidth: 1/2,
        borderColor: '#ccc',
        height: 40,
        alignItems: 'center',
        justifyContent: 'space-between',
        width: width,
        paddingHorizontal: 10,
        backgroundColor: Color.introButtonsBackground
    },
    btnText: {
        color: 'black',
        margin: 10,
        fontSize: 14
    },
    indicatorWrap: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    bottomView: {
        // backgroundColor: Platform.OS == 'android' ? 'transparent' : 'transparent'
        backgroundColor: 'transparent'
    }
});
