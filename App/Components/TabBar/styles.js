/** @format */

import {Dimensions, Platform, StyleSheet} from 'react-native'

import {Color} from '@common'

const {width} = Dimensions.get('window')


export default StyleSheet.create({
    tabbar: {
        height: 49,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        //backgroundColor: 'red',
        //borderTopWidth: 1/2,
        //borderTopColor: Color.tabbarTopBorder,

        borderRadius:25, 
        backgroundColor:Color.tabbar,
        position:'absolute',
        bottom: 20,
        alignItems:'center',
        borderTopWidth: 0,
        right:5,
        left:5,
        padding:10,
        width: width-10,
        height: 54,
        zIndex: 8,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.15,
        shadowRadius: 4.84,
        elevation: 5,
    },
    tab: {
        alignSelf: 'stretch',
        flex: 1,    
        alignItems: 'center',
        ...Platform.select({
            ios: {
                justifyContent: 'center',
                paddingTop: 0,
            },
            android: {
                justifyContent: 'center',
            },
        }),
    },
})
