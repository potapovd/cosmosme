/** @format */

import { Dimensions, StyleSheet } from 'react-native'

import { Color } from '@common'
const { width, height } = Dimensions.get('window')

export default StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: Color.postsListBackground,
  },
  flatlist: {
    // flex:1,
    // flexWrap: 'wrap',
    // flexDirection: 'row',
     backgroundColor: Color.postsListBackground,
    paddingBottom: 20,
    paddingTop: 30,

  },
  more: {
    width,
    alignItems: 'center',
    marginBottom: 10,
    marginTop: 10,
  },
  spinView: {
    width,
    backgroundColor: 'transparent',
    flex: 1,
    height,
    paddingTop: 20,
  },
  navbar: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    backgroundColor: 'white',
    borderBottomColor: '#dedede',
    borderBottomWidth: 0,
    height: 40,
    justifyContent: 'center',
  },
  contentContainer: {
    paddingTop: 40,
    backgroundColor:'red'
  },
  title: {
    color: '#333333',
  },
  row: {
    height: 300,
    width: null,
    marginBottom: 1,
    padding: 16,
    //backgroundColor: 'transparent',
  },
  rowText: {
    color: 'white',
    fontSize: 18,
  },
})
