import {Color, Config, Constants, Device} from "@common";
import {Dimensions, Platform, StyleSheet} from 'react-native';
const {width} = Dimensions.get("window")

export default StyleSheet.create({
  body: {
    height:50,
    ...Platform.select({
      ios: {
        zIndex: 9
      }
    })
  },
  headerLabel: {
    color: Color.categoriesColor,
    fontSize: 26,
    fontWeight: "300",
    fontFamily: Constants.fontHeader,
    marginBottom: 0,
    marginLeft: 40,
    // backgroundColor: 'transparent',
    position: 'absolute',
    top: 40,
    zIndex: 9,
    ...Platform.select({
      android: {
        //paddingTop: 0
      },
    }),
  },
  headerImage: {
    marginBottom: 0,
    marginLeft: 0,
    //width: width * 1 / 5,
    width:width - 20,
    height: 60,
    resizeMode: 'contain',
    position: 'absolute',
    top: 10,
    left: 10,
    zIndex: 9,
    paddingBottom:0,
    ...Platform.select({
      android: { height: 55, },
    }),
  },
  headerView: {
    width,
    height: 80,
    shadowColor: "#000",
    shadowOpacity: 0.1,
    shadowRadius: 2,
    shadowOffset: {width: 0, height: 3},
    borderBottomWidth: 0.5,
    borderBottomColor: Color.headerViewBottomBorder,
    //elevation: 5,
    zIndex:9,
    opacity: 1,
    backgroundColor: Color.headerBackgroundColor
  },
  flatlist: {
    paddingTop: 40
  },
  homeMenu: {
    marginLeft: 5,
    top:  42,
    position: 'absolute',
    zIndex: 9,
  },
  headerRight: {
    position: 'absolute',
    zIndex: 9,

    top: 58,
    right: 10,
    ...Platform.select({
      android: {
        top: 54
      },
    }),
  }
});
