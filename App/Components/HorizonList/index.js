/** @format */

import Animated, {color} from 'react-native-reanimated';
import {AnimatedHeader, PostLayout} from '@components';
import {Color, Constants, Images, Languages} from '@common';
import {Dimensions, FlatList, Text, TouchableOpacity, View} from 'react-native';
import React, {PureComponent} from 'react';

import Banner from './Banner';
import Icon from '@expo/vector-icons/Entypo';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {fetchPostsByTag} from '@redux/actions';
import styles from './styles';

const {width} = Dimensions.get('window')


const AnimatedListView = Animated.createAnimatedComponent(FlatList);

class HorizonList extends PureComponent {
    static propTypes = {
        layouts: PropTypes.object,
        onViewPost: PropTypes.func,
        onShowAll: PropTypes.func,
        config: PropTypes.object,
        index: PropTypes.number,
        fetchPostsByTag: PropTypes.func,
        horizontal: PropTypes.bool,
        deviceLanguage: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this.page = 1;
        this.defaultList = [
            {
                "title": {
                    "rendered": `${Languages.loading}`
                },
                "better_featured_image": {
                    "source_url": `${Images.imageHolder}`
                },
                "date": "2020-10-25T21:09:22",
                "excerpt": {
                    "rendered": ""
                },
            }
        ];

        this.state = {
            scrollY: new Animated.Value(0),
        };
    }

    componentDidMount() {
        this.fetchPost();
    }

    onViewPost = (item, index) => {
        this.props.onViewPost(item, index, this.props.layouts.list);
    };


    fetchPost = () => {
        const {config, index, fetchPostsByTag, deviceLanguage} = this.props;
        fetchPostsByTag(this.page, config.tags, config.categories, index, deviceLanguage);
    };

    nextPosts = () => {
        this.page += 1;
        !this.props.layouts.finish && this.fetchPost();
    };

    viewAll = () => {
        const {index, config, onShowAll} = this.props;
        onShowAll({index, config});
    };

    renderItem = ({item, index}) => {
        const {layouts, horizontal, config} = this.props;
        const {layout, textColor, row} = config;

        const isFlexibleColumn =
            layout === Constants.Layout.threeColumn ||
            layout === Constants.Layout.column ||
            layout === Constants.Layout.flexColumn;
        let newLayout =
            !horizontal && isFlexibleColumn ? Constants.Layout.column : layout;
        
        const list =
            typeof layouts !== 'undefined' && layouts.list !== 0
                ? layouts.list
                : this.defaultList;
        let numOfLine = row || 1;
        const newIndex = index * numOfLine;

        if (typeof list === 'undefined' || newIndex >= list.length) {
            return <View/>;
        }

        if (newIndex + numOfLine > list.length) {
            numOfLine = list.length - newIndex;
        }
        return (
            <View>
                {Array.apply(0, Array(numOfLine)).map((_, index) => {
                    const item = list[newIndex + index];
                    return (
                        <PostLayout
                            style={{ backgroundColor:'green'}}
                            post={item}
                            key={`post-${index}`}
                            config={config}
                            textColor={textColor}
                            onViewPost={() => this.onViewPost(item, newIndex + index)}
                            layout={newLayout}
                            deviceLanguage={this.props.deviceLanguage}
                        />
                    );
                })}
            </View>
        );
    };

    renderHeader = () => {
        const {config} = this.props;
        let OnPress;
        if (config.layout !== 4) {
            OnPress = this.viewAll
        }
        return (
            <TouchableOpacity
                activeOpacity={0.9}
                style={[styles.header]}
                onPress={OnPress}
            >
                <Icon
                    style={styles.icon}
                    color={Color.homeCategoriesLeftIconColor}
                    size={20}
                    name={config.icon}
                />
                <Text
                    style={[
                        styles.tagHeader,
                        {color: Color.homeCategoriesColor}
                    ]}>
                    {config.name}
                </Text>
                <Icon
                    style={styles.icon}
                    color={Color.homeCategoriesRightIconColor}
                    size={24}
                    name={Constants.RTL ? 'chevron-small-left' : 'chevron-small-right'}
                />
            </TouchableOpacity>
        );
    };

    renderAnimatedHeader = () => {
        const {config, goBack} = this.props;
        return (
            <AnimatedHeader
                goBack={goBack}
                label={config.name}
                scrollY={this.state.scrollY}
            />
        );
    };

    render() {
        const {layouts, horizontal, config} = this.props;
        const isPaging = !!config.paging;

        const list =
            typeof layouts !== 'undefined' && layouts.list
                ? layouts.list
                : this.defaultList;
        //console.clear()


        if (typeof list === 'undefined' || list.length === 0) return <View/>;

        return (
            <View
                // style={{flex:1}}
                style={
                    [
                        styles.flatWrap,
                        config.color && {backgroundColor: config.bgColor},
                    ]
                }
            >
                {!horizontal && this.renderAnimatedHeader()}

                {config.name && horizontal && this.renderHeader()}

                {config.layout == Constants.Layout.banner && (
                    <Banner
                        list={list}
                        config={config}
                        onViewPost={this.onViewPost}/>
                )}

                {config.layout != Constants.Layout.banner && (
                    <AnimatedListView
                        contentContainerStyle={[styles.hList, !horizontal && styles.vList]}
                        data={list}
                        keyExtractor={(item, index) => `${item.id || index}`}
                        renderItem={this.renderItem}
                        showsHorizontalScrollIndicator={!horizontal}
                        horizontal={horizontal}
                        pagingEnabled={horizontal && isPaging}
                        onEndReached={!horizontal && this.nextPosts}
                        scrollEventThrottle={1}
                        onScroll={Animated.event(
                            [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                            {useNativeDriver: true}
                        )}
                    />

                )}
            </View>
        );
    }
}

const mapStateToProps = ({layouts, language}, ownProp) => {
    const index = ownProp.index;
    const deviceLanguage = language.language.languageTag;
    return {layouts: layouts[index], deviceLanguage: deviceLanguage};
};

export default connect(
    mapStateToProps,
    {fetchPostsByTag}
)(HorizonList);


//
// const mapStateToProps = (state) => {
//     const deviceLanguage = state.language.language.languageTag;
//     return {deviceLanguage};
// };
// export default connect(mapStateToProps)(Horizontal);



