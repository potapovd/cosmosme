/** @format */

import {Color, Constants} from '@common'
import {Dimensions, StyleSheet} from 'react-native'

const {width} = Dimensions.get('window')
const vw = width / 100

export default StyleSheet.create({
    panel: {
        backgroundColor: Color.readLaterBackgroundColor,
        borderColor: Color.readLaterBorderColor,
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        position: 'relative',
    },
    title: {
        width: vw * 70,
        flex: 1,
    },
    image: {
        marginTop: 12,
        marginLeft: 8,
        marginRight: 8,
        marginBottom: 8,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        width: vw * 30,
        height: vw * 30 - 20,
        resizeMode: 'cover',
        borderRadius: 6,
    },

    imageListOut: {
        // backgroundColor:'red',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.35,
        shadowRadius: 3.84,
        elevation: 5,
        position: 'relative'
    },

    name: {
        fontSize: 16,
        marginLeft: 2,
        marginTop: 6,
        marginRight: 16,
        paddingRight: 16,
        color: '#333',
        fontWeight: '300',
        textAlign: 'left'
    },
    time: {
        marginLeft: Constants.RTL ? 25 : 10,
        marginRight: 8,
        color: '#999',
        fontSize: 11,
        marginBottom: 10,
        marginTop: 6,
        backgroundColor: 'transparent',
        textAlign: 'left'
    },

    iconPlay: {
        color: Color.playButtonArrow,
        backgroundColor: 'transparent',
        marginTop: 10,
        marginRight: 18,
        marginBottom: 10,
        marginLeft: 22,
        zIndex: 9999,
        width: 24,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.35,
        shadowRadius: 3.84,
        elevation: 5,
    },
    iconVideo: {
        alignItems: 'center',
        justifyContent: 'center',
        top: 35,
        left: 37,
        zIndex: 999,
        width: 46,
        height: 46,
        position: 'absolute',
        backgroundColor: Color.playButton,
        borderRadius: 36,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.35,
        shadowRadius: 3.84,
        elevation: 5,
    },
})
