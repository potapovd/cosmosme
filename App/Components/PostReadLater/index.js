/** @format */

import { Image, Text, TouchableOpacity, View } from 'react-native'
import React, { PureComponent } from 'react'

import Icon from "@expo/vector-icons/SimpleLineIcons";
import { ImageCache } from '@components';
import PropTypes from 'prop-types'
import Swipeout from 'react-native-swipeout'
import TimeAgo from 'react-native-timeago'
import { Tools } from '@common'
import User from '@services/User'
import css from './styles'

export default class PostReadLater extends PureComponent {
  static propTypes = {
    post: PropTypes.object,
    onViewPost: PropTypes.func,
  }

  constructor(props) {
    super(props)
    this.state = { isRemove: false }
  }

  removePost = (post) => {
    User.removePost(post)
    this.setState({ isRemove: true })
  }

  onOpen = () => {}

  render() {
    if (this.state.isRemove) {
      return null
    }

    const swipeBtns = [
      {
        text: 'Удалить',
        backgroundColor: '#FF4455',
        borderColor: '#fff',
        borderWidth: '1',
        onPress: () => {
          this.removePost(this.props.post)
        },
      },
    ]
    const { onViewPost, post } = this.props
    const imageURL = Tools.getImage(post)
    const postTitle =
      typeof post.title.rendered !== 'undefined' ? post.title.rendered : ''

    return (
      <Swipeout
        onOpen={this.onOpen}
        style={{ backgroundColor: 'transparent' }}
        right={swipeBtns}
      >
        <View style={css.panel} onPress={onViewPost}>
          <TouchableOpacity onPress={onViewPost}>
            <View style={css.imageListOut}>
              <ImageCache source={{ uri: imageURL }} style={css.image} />
              {post.format == 'audio' && (
                <View
                    style={[
                        css.iconVideo,
                        //width && {left: width / 2 - 15},
                        //height && {top: height / 3 - 10},
                    ]}>
                    <Icon name="music-tone-alt" size={20} style={css.iconPlay}/>
                </View>
            )}
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={onViewPost}>
            <View style={css.title}>
              <Text style={css.name}>{Tools.formatText(postTitle, 150)}</Text>
              {/* <Text style={css.time}>
                <TimeAgo time={post.date} hideAgo={false} />
              </Text> */}
            </View>
          </TouchableOpacity>
        </View>
      </Swipeout>
    )
  }
}
