/** @format */

import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {
    View,
    Image,
    Dimensions,
    Linking,
    WebBrowser,
} from 'react-native'
import HTML from 'react-native-render-html'
import {Tools, Constants, error, warn, Config} from '@common'
import sanitizeHtml from 'sanitize-html';

//import SoundPlayer from 'react-native-sound-player'
// import TrackPlayer, {
//     play,
//     useTrackPlayerProgress,
//     usePlaybackState,
//     RNTrackPlayer,
//     useTrackPlayerEvents
// } from 'react-native-track-player';


const {height: PageHeight, width: PageWidth} = Dimensions.get('window')
import {WebView} from "react-native-webview";


// function ProgressBar() {
//     const progress = useTrackPlayerProgress();
//
//     return (
//
//             <View style={{
//                 height: 1,
//                 width: "90%",
//                 marginTop: 10,
//                 flexDirection: "row"
//             }}>
//                 <View style={{flex: progress.position, backgroundColor: "red"}}/>
//
//
//                 <View
//                     style={{
//                         flex: progress.duration - progress.position,
//                         backgroundColor: "grey"
//                     }}
//                 />
//             </View>
//     );
// }

export default class Index extends PureComponent {

    static propTypes = {
        html: PropTypes.any,
    }

    constructor(props) {
        super(props)
        this.state = {fontSize: Constants.fontText.size}
    }

    async UNSAFE_componentWillMount() {
        const fontSize = await Tools.getFontSizePostDetail()

        this.setState({
            fontSize,
        })
    }

    onLinkPress = (url) => {
        if (typeof WebBrowser !== 'undefined') {
            WebBrowser.openBrowserAsync(url)
        } else {
            Linking.canOpenURL(url)
                .then((supported) => {
                    if (!supported) {
                    } else {
                        return Linking.openURL(url)
                    }
                })
                .catch((err) => error('An error occurred', err))
        }
    }

    // playSong(){
    //    try {
    //         // play the file tone.mp3
    //         SoundPlayer.playSoundFile('tone', 'mp3')
    //         // or play from url
    //         SoundPlayer.playUrl('https://lili.one/wp-content/uploads/2020/11/vmanmusic-dle-yaman-remix-2019.mp3')
    //     } catch (e) {
    //         console.log(`cannot play the sound file`, e)
    //     }
    // }
    // async getInfo() { // You need the keyword `async`
    //     try {
    //         const info = await SoundPlayer.getInfo() // Also, you need to await this because it is async
    //         console.log('getInfo', info) // {duration: 12.416, currentTime: 7.691}
    //     } catch (e) {
    //         console.log('There is no song playing', e)
    //     }
    // }
    //
    // onPressPlayButton() {
    //     this.playSong()
    //     this.getInfo()
    // }
    componentWillUnmount() {
        // Removes the event handler
        //this.onTrackChange.remove();
        //console.log('foo')
    }


    render() {

        //TrackPlayer.addEventListener('remote-duck', () => { TrackPlayer.destroy() });

        // TrackPlayer.setupPlayer().then(async () => {
        //     // Adds a track to the queue
        //     await TrackPlayer.add({
        //         id: 'trackId',
        //         url: "https://lili.one/wp-content/uploads/2020/11/vmanmusic-dle-yaman-remix-2019.mp3",
        //         title: 'Track Title',
        //         artist: 'Track Artist',
        //         pitchAlgorithm: TrackPlayer.PITCH_ALGORITHM_VOICE,
        //         //artwork: require('./static/refresh.png'),
        //     });
        //     TrackPlayer.updateOptions({
        //         capabilities: [
        //             TrackPlayer.CAPABILITY_PLAY,
        //             TrackPlayer.CAPABILITY_PAUSE,
        //             //TrackPlayer.CAPABILITY_STOP
        //         ],
        //         compactCapabilities: [
        //             TrackPlayer.CAPABILITY_PLAY,
        //             TrackPlayer.CAPABILITY_PAUSE,
        //             //TrackPlayer.CAPABILITY_STOP
        //         ]
        //     });
        // });
        // const playHandel = () => {
        //     TrackPlayer.play().then(() => {
        //         console.log('played');
        //     }).catch(err => {
        //         console.log(err)
        //     });
        // };
        // const pauseHandel = () => {
        //     console.log('push');
        //     TrackPlayer.pause();
        // };
        //
        // const togglePlay = async () => {
        //     //const isPlaying = await TrackPlayer.isPlaying()
        //     let state = await TrackPlayer.getState();
        //     console.log("state " + state);
        //
        //
        //     if (state == 'playing') {
        //         return TrackPlayer.pause()
        //     } else {
        //         return TrackPlayer.play()
        //     }
        // }

// const start = async () => {
//     // Set up the player
//     await TrackPlayer.setupPlayer();
//
//     // Add a track to the queue
//     await TrackPlayer.add({
//         id: 'trackId',
//         url: 'https://lili.one/wp-content/uploads/2020/11/vmanmusic-dle-yaman-remix-2019.mp3',
//         title: 'Track Title',
//         artist: 'Track Artist',
//         //artwork: require('track.png')
//     });
//
//     // Start playing it
//     await TrackPlayer.play();
// };


        const htmlContent = Config.EnableSanitizeHtml ? sanitizeHtml(this.props.html, {
            // allowedTags: ['b', 'p', 'i', 'img', 'em', 'strong', 'a', 'span', 'font', 'br', 'audio'],
            // allowedAttributes: {
            //     'a': ['href'],
            //     'img': ['src', 'alt', 'width', 'height'],
            //     'audio': ['src', 'controls'],
            // },
            allowedIframeHostnames: ['www.youtube.com', 'lili.one']
        }) : this.props.html

        const fontSize = this.state.fontSize
            ? this.state.fontSize
            : Constants.fontText.size

        const tagsStyles = {
            a: {color: '#333', fontSize},
            strong: {color: '#333', fontSize, fontWeight: '700'},
            p: {color: '#333', marginBottom: 5, fontSize, lineHeight: 24},
            em: {fontStyle: 'italic', fontSize},
            video: {marginBottom: 5},
            img: {resizeMode: 'cover'},
            ul: {color: '#333'},
            li: {color: '#333'},
        }


        const renderers = {
            img: (htmlAttribs, children, convertedCSSStyles, passProps) => {
                const {src, width = 1200, height = 800} = htmlAttribs
                if (!src) {
                    return false
                }

                const newWidth = Dimensions.get('window').width - 20
                const newHeight = height * newWidth / width
                return (
                    <Image
                        key={passProps.key}
                        source={{uri: src}}
                        style={{
                            width: newWidth,
                            height: newHeight,
                            resizeMode: 'contain',
                        }}
                    />
                )
            },
            iframe: (htmlAttribs, children, convertedCSSStyles, passProps) => {
                if (htmlAttribs.src) {
                    const newWidth = PageWidth
                    const width = htmlAttribs.width
                    const height = htmlAttribs.height
                    const newHeight = height > 0 ? height * newWidth / width : width * 0.7
                    const url = htmlAttribs.src

                    return (
                        <WebView
                            key={`webview-${passProps.key}`}
                            source={{uri: url}}
                            allowsInlineMediaPlayback
                            mediaPlaybackRequiresUserAction={false}
                            javaScriptEnabled
                            scrollEnabled={false}
                            style={{
                                width: PageWidth,
                                left: -12,
                                height: newHeight + 15,
                            }}
                        />
                    )
                }
            },
        }

        //warn(['content:', htmlContent])


        return (
            <View style={{padding: 12}}>
                {/*<Button*/}
                {/*    onPress={playHandel}*/}
                {/*    title="Play"*/}
                {/*/>*/}
                {/*<Button*/}
                {/*    onPress={pauseHandel}*/}
                {/*    title="Pause"*/}
                {/*/>*/}
                {/*<ProgressBar/>*/}
                {/*<Button*/}
                {/*    onPress={togglePlay}*/}
                {/*    title="PP"*/}
                {/*/>*/}
                <HTML
                    html={Constants.RTL ? `<div style="text-align: left;">${htmlContent}</div>` : htmlContent}
                    ignoredStyles={['font-family']}
                    renderers={renderers}
                    imagesMaxWidth={PageWidth}
                    tagsStyles={tagsStyles}
                    onLinkPress={(evt, href) => this.onLinkPress(href)}
                    staticContentMaxWidth={PageWidth}
                />

                {/*https://go.transportili.app/static/sounds/ring.mp3*/}


            </View>
        )
    }
}
