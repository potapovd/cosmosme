/** @format */

import React from 'react'
import Icon from 'react-native-vector-icons/Ionicons';

const IconPure = ({icon, tintColor, size, css}) => {
    return (
        <Icon name={icon} size={size ? size : 28} color={tintColor} style={css}/>
    )
}

export default IconPure
