import React from 'react'
import {
    View,
    TouchableOpacity,
    Image,
    Platform
} from 'react-native'

import {Color, Images} from '@common'
import Indicator from './Indicator'
import {TabBarIcon} from '@components'

import styles from './style'



class HeaderDetail extends React.Component {
    render() {
        let {onBack, onMore, parentPosts, scrollX} = this.props

        const emptyClick = () => {
            return false
        }

        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.button} hitSlop={{top: 10, left: 10, right: 10, bottom: 10}}
                                  onPress={onBack}>
                    {/*<Image source={Images.WhiteBackIcon} style={styles.backIcon} />*/}
                    <TabBarIcon icon={Images.icons.backIcon} tintColor={Color.headerBackButton}/>
                </TouchableOpacity>
                {/*{Platform.OS === "ios" && <Indicator items={parentPosts} scrollX={scrollX} />}*/}
                <Indicator items={parentPosts} scrollX={scrollX}/>
                {/*<TouchableOpacity style={styles.button} hitSlop={{ top: 10, left: 10, right: 10, bottom: 10 }} onPress={emptyClick}>*/}
                {/*<TouchableOpacity style={styles.button} hitSlop={{top: 10, left: 10, right: 10, bottom: 10}}*/}
                {/*                  onPress={onMore}>*/}
                {/*    /!*<Image source={Images.WhiteMoreIcon} style={styles.icon} />*!/*/}
                {/*    <TabBarIcon icon={Images.icons.postMenuIcon} tintColor={Color.headerBackButton}/>*/}
                {/*</TouchableOpacity>*/}
            </View>
        )
    }
}

export default HeaderDetail
