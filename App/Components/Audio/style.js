import {Color, Config, Constants, Device} from "@common";
import {Dimensions, Platform, StyleSheet} from 'react-native';

const {width} = Dimensions.get("window")

export default StyleSheet.create({

    progressLineOut: {
        height: 1,
        width: '100%',
        marginTop: 10,
        flexDirection: "row"
    },
    progressLineActive: {
        backgroundColor: Color.playButton
    },
    progressLinePath: {
        backgroundColor: "#ccc",
        width: '100%',
    },
    timerRow: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop:10
    },
    timerCounter: {
        fontWeight: '500',
    },
    blockControls: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        marginBottom:20
    },
    lottieSpectrumOut:{
        width,
        //height:300,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    lottieSpectrum:{
        width: width-20,
    },
    // buttonPlay:{
    //     width:50,
    //     height: 50,
    //     backgroundColor: 'gold',
    //     borderRadius: 25
    // }
    iconVideo: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',

        //top: Constants.Window.headerHeight / 2 - 30,
        //left: width / 2 - 30,
        //zIndex: 999,
        width: 60,
        //position: 'absolute',
        // backgroundColor: Color.playButton,
        // height: 60,
        // borderRadius: 40,
        // alignSelf: 'center',
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 2,
        // },
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,
        // elevation: 5,
    },
    iconPlay: {
        //alignSelf: 'center',
        color: Color.audioContols,
        backgroundColor: 'transparent',
        marginLeft:-2,
        marginTop:-2,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        //marginLeft:5,
        //backgroundColor: 'red',
        // marginTop: 22,
        // marginRight: 18,
        // marginBottom: 18,
        // marginLeft: 26,
        // zIndex: 9999,
        // width: 30,
        // height: 30,
        //  flex: 1,
        //  alignItems: 'center',
        //  justifyContent: 'center',
    },


    // body: {
    //   height:30,
    //   ...Platform.select({
    //     ios: {
    //       zIndex: 9
    //     }
    //   })
    // },
    // headerLabel: {
    //   color: Color.categoriesColor,
    //   fontSize: 28,
    //   fontWeight: "300",
    //   fontFamily: Constants.fontHeader,
    //   marginBottom: 0,
    //   marginLeft: 10,
    //   // backgroundColor: 'transparent',
    //   position: 'absolute',
    //   top: 40,
    //   zIndex: 9,
    //   ...Platform.select({
    //     android: {
    //       //paddingTop: 0
    //     },
    //   }),
    // },
    // headerImage: {
    //   marginBottom: 0,
    //   marginLeft: 0,
    //   //width: width * 1 / 5,
    //   width:width - 20,
    //   height: 60,
    //   resizeMode: 'contain',
    //   position: 'absolute',
    //   top: 10,
    //   left: 10,
    //   zIndex: 9,
    //   paddingBottom:0,
    //   ...Platform.select({
    //     android: { height: 55, },
    //   }),
    // },
    // headerView: {
    //   width: width,
    //   height: 50,
    //   //shadowColor: "#000",
    //   //shadowOpacity: 0.1,
    //   //shadowRadius: 2,
    //   //shadowOffset: {width: 0, height: 3},
    //   borderBottomWidth: 0.5,
    //   borderBottomColor: Color.headerViewBottomBorder,
    //   //elevation: 5,
    //   zIndex:9,
    //   opacity: 1,
    //   backgroundColor: Color.headerBackgroundColor
    // },
    // flatlist: {
    //   paddingTop: 40
    // },
    // homeMenu: {
    //   marginLeft: 10,
    //   top:  12,
    //   position: 'absolute',
    //   zIndex: 9,
    // },
    // headerRight: {
    //   position: 'absolute',
    //   zIndex: 9,
    //
    //   top: 58,
    //   right: 10,
    //   ...Platform.select({
    //     android: {
    //       top: 54
    //     },
    //   }),
    // }
});
