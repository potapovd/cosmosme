/** @format */

import {
    Button,
    Dimensions,
    Image,
    Linking,
    Text,
    TouchableOpacity,
    View,
    WebBrowser
} from 'react-native'
import {Config, Constants, Tools, error, warn} from '@common'
import React, {PureComponent} from 'react'
//import SoundPlayer from 'react-native-sound-player'
import TrackPlayer, {
    RNTrackPlayer,
    play,
    usePlaybackState,
    useTrackPlayerEvents,
    useTrackPlayerProgress
} from 'react-native-track-player';

import Icon from '@expo/vector-icons/Ionicons'
import {Lottie} from "@expo";
import PropTypes from 'prop-types'
import styles from './style'

const audioSpectrum = require('@images/lottie/audio_spectrum');
const {height: PageHeight, width: PageWidth} = Dimensions.get('window')

function fancyTimeFormat(duration) {
    // Hours, minutes and seconds
    var hrs = ~~(duration / 3600);
    var mins = ~~((duration % 3600) / 60);
    var secs = ~~duration % 60;

    // Output like "1:01" or "4:03:59" or "123:03:59"
    var ret = "";

    if (hrs > 0) {
        ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
    }

    ret += "" + mins + ":" + (secs < 10 ? "0" : "");
    ret += "" + secs;
    return ret;
}

function ProgressBar() {
    const progress = useTrackPlayerProgress();
    return (
        <View>
            {/* <View style={styles.progressLineOut}>
                <View style={[styles.progressLineActive, {flex: progress.position}]}/>
                <View
                    style={
                        [styles.progressLinePath, {flex: progress.duration - progress.position,}]
                    }
                />
            </View> */}
        
            <View style={styles.timerRow}>
                <Text style={styles.timerCounter}>{fancyTimeFormat(progress.duration)}</Text>
                <Text style={styles.timerCounter}>{fancyTimeFormat(progress.position)}</Text>
            </View>
        </View>
    );
}


export default class Index extends PureComponent {


    static propTypes = {
        src: PropTypes.string,
        image: PropTypes.string,
        title: PropTypes.string,
    }

    constructor(props) {
        super(props)
        var tpState = TrackPlayer.getState();
        this.state = {
            playerButton: "play",
            playback: tpState,
			isPlaying: tpState == TrackPlayer.STATE_PLAYING ? true : false,
			isBuffering: tpState == TrackPlayer.STATE_BUFFERING ? true : false,
        }
    }

    async UNSAFE_componentWillMount() {
        // const fontSize = await Tools.getFontSizePostDetail()
        //
        // this.setState({
        //     fontSize,
        // })


    }


    // playSong(){
    //    try {
    //         // play the file tone.mp3
    //         SoundPlayer.playSoundFile('tone', 'mp3')
    //         // or play from url
    //         SoundPlayer.playUrl('https://lili.one/wp-content/uploads/2020/11/vmanmusic-dle-yaman-remix-2019.mp3')
    //     } catch (e) {
    //         console.log(`cannot play the sound file`, e)
    //     }
    // }
    // async getInfo() { // You need the keyword `async`
    //     try {
    //         const info = await SoundPlayer.getInfo() // Also, you need to await this because it is async
    //         console.log('getInfo', info) // {duration: 12.416, currentTime: 7.691}
    //     } catch (e) {
    //         console.log('There is no song playing', e)
    //     }
    // }
    //
    // onPressPlayButton() {
    //     this.playSong()
    //     this.getInfo()
    // }
    hpl = ()=>{
        this.animation.play()
    }
    componentDidMount() {
        //this.animation.play();
        //let _this = this
        this.onPlaybackState = TrackPlayer.addEventListener('playback-state', async (data) => {
			var tpState = data.state;
			
			this.setState({
				playback: tpState,
			});

			if(tpState == TrackPlayer.STATE_PLAYING)
			{
				this.setState({
					isPlaying: true,
					isBuffering: false,
				},()=>{this.playAnimation()});
			}
			else if(tpState == TrackPlayer.STATE_BUFFERING)
			{
				this.setState({
					isPlaying: false,
					isBuffering: true,
				},()=>{this.stopAnimation()});
			}
			else
			{
				this.setState({
					isPlaying: false,
					isBuffering: false,
				},()=>{this.stopAnimation()});
			}
        });
    }

    resetAnimation = () => {
        this.animation.reset();
        this.animation.play();
    };
    playAnimation = () => {
        this.animation.play();
    };
    stopAnimation = () => {
        this.animation.pause();
    };
    componentWillMount() {
        // Removes the event handler
        //this.onTrackChange.remove();
        //console.log('foo')

        //const currentTrack = await TrackPlayer.getCurrentTrack();
        //console.log("this.props.src "+this.props.src, currentTrack)
        TrackPlayer.setupPlayer().then(async () => {
            // Adds a track to the queue
            await TrackPlayer.reset();
            // await TrackPlayer.add({
            //     id: this.props.src,
            //     //url: "https://lili.one/wp-content/uploads/2020/11/vmanmusic-dle-yaman-remix-2019.mp3",
            //     url: this.props.src,
            //     title: 'Track Title',
            //     artist: 'Track Artist',
            //     pitchAlgorithm: TrackPlayer.PITCH_ALGORITHM_VOICE,
            //     //artwork: require('./static/refresh.png'),
            // });
            //await TrackPlayer.play();
            TrackPlayer.updateOptions({
                capabilities: [
                    TrackPlayer.CAPABILITY_PLAY,
                    TrackPlayer.CAPABILITY_PAUSE,
                    //TrackPlayer.CAPABILITY_STOP
                ],
                compactCapabilities: [
                    TrackPlayer.CAPABILITY_PLAY,
                    TrackPlayer.CAPABILITY_PAUSE,
                    //TrackPlayer.CAPABILITY_STOP
                ]
            });
        });
    }


    togglePlay = async () => {
        //const isPlaying = await TrackPlayer.isPlaying()

        let state = await TrackPlayer.getState();
        let currentTrack = await TrackPlayer.getCurrentTrack();

        //return  false
        


        if (currentTrack == this.props.src) {
            if (state == 'playing') {
                this.setState({playerButton: "play"})
                //this.animation.pause();
                return TrackPlayer.pause()
            } else {
                this.setState({playerButton: "pause"})
                //this.animation.play();
                return TrackPlayer.play()
            }
        } else {
            await TrackPlayer.reset();
            await TrackPlayer.add({
                id: this.props.src,
                url: this.props.src,
                title: this.props.title,
                artwork: {uri: this.props.image},
                artist: Constants.AppLongName,
                //pitchAlgorithm: TrackPlayer.PITCH_ALGORITHM_VOICE,
            });
               // return  false
            await TrackPlayer.play();
            //this.animation.play();
            this.setState({playerButton: "pause"})

        }


        //
        //


    }
    rewind = async () => {
        // TrackPlayer.updateOptions({
        //     jumpInterval: -10
        // });
        let newPosition = await TrackPlayer.getPosition();
        let duration = await TrackPlayer.getDuration();
        newPosition -= 10;
        if (newPosition < 0) {
            newPosition = 0;
        }
        TrackPlayer.seekTo(newPosition);
    }
    forward = async () => {
        // TrackPlayer.updateOptions({
        //     jumpInterval:10
        // });

        let newPosition = await TrackPlayer.getPosition();
        let duration = await TrackPlayer.getDuration();
        newPosition += 10;
        if (newPosition > duration) {
            newPosition = duration;
        }
        TrackPlayer.seekTo(newPosition);
    }

    Controls = () => {
        return (
            <View style={styles.blockControls}>
                {/*<Button*/}
                {/*    onPress={() => this.minus()}*/}
                {/*    title={'-15'}*/}
                {/*/>*/}
                 <TouchableOpacity
                    activeOpacity={0.9}
                    onPress={() => this.rewind()}
                    // title={this.state.playerButton}
                >
                    {/*<Text style={styles.buttonPlay}>{this.state.playerButton}</Text>*/}
                    {/*<Icon name="forward" size={22} style={[styles.iconPlay, {height: 25, width: 25}]}/>*/}
                    <Icon name="ios-play-back" size={32} style={[styles.iconPlay, {height: 32, width: 32}]}/>
                    {/*<Ionicons name="ios-play-forward" size={24} color="black" />*/}
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.9}
                    onPress={() => this.togglePlay()}

                    // title={this.state.playerButton}
                >
                    {/*<Text style={styles.buttonPlay}>{this.state.playerButton}</Text>*/}
                    <View style={styles.iconVideo}>
                        {this.state.playerButton=='play'?
                            <Icon name="ios-play" size={62} style={[styles.iconPlay, {height: 62, width: 62}]}/>
                            :
                            <Icon name="ios-pause" size={62} style={[styles.iconPlay, {height: 62, width: 62}]}/>
                        }
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.9}
                    onPress={() => this.forward()}

                    // title={this.state.playerButton}
                >
                    {/*<Text style={styles.buttonPlay}>{this.state.playerButton}</Text>*/}
                    <Icon name="ios-play-forward" size={32} style={[styles.iconPlay, {height: 32, width: 32}]}/>
                </TouchableOpacity>
            </View>
        )
    }


    render() {
//const playbackState = usePlaybackState();
        //TrackPlayer.addEventListener('remote-duck', () => { TrackPlayer.destroy() });

        // TrackPlayer.setupPlayer().then(async () => {
        //     // Adds a track to the queue
        //     await TrackPlayer.add({
        //         id: 'trackId',
        //         //url: "https://lili.one/wp-content/uploads/2020/11/vmanmusic-dle-yaman-remix-2019.mp3",
        //         url: this.props.src,
        //         title: 'Track Title',
        //         artist: 'Track Artist',
        //         pitchAlgorithm: TrackPlayer.PITCH_ALGORITHM_VOICE,
        //         //artwork: require('./static/refresh.png'),
        //     });
        //     TrackPlayer.updateOptions({
        //         capabilities: [
        //             TrackPlayer.CAPABILITY_PLAY,
        //             TrackPlayer.CAPABILITY_PAUSE,
        //             //TrackPlayer.CAPABILITY_STOP
        //         ],
        //         compactCapabilities: [
        //             TrackPlayer.CAPABILITY_PLAY,
        //             TrackPlayer.CAPABILITY_PAUSE,
        //             //TrackPlayer.CAPABILITY_STOP
        //         ]
        //     });
        // });
        // const playHandel = () => {
        //     TrackPlayer.play().then(() => {
        //         console.log('played');
        //     }).catch(err => {
        //         console.log(err)
        //     });
        // };
        // const pauseHandel = () => {
        //     console.log('push');
        //     TrackPlayer.pause();
        // };


        return (
            <View style={{padding: 12}}>


                {this.Controls()}
                <View style={styles.lottieSpectrum}>
                    <Lottie
                        ref={animation => {
                            this.animation = animation;
                        }}
                        style={styles.lottieSpectrum}
                        loop={true}
                        source={audioSpectrum}
                        //style={styles.lottie}
                        //source={noImg}
                    />
                </View>
                
                <ProgressBar/>


                {/*https://go.transportili.app/static/sounds/ring.mp3*/}


            </View>
        )
    }
}
