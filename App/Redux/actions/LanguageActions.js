import {
    SET_LANGUAGE,
} from '@redux/types';

export const setDeviceLanguage = (data) => {
    return (dispatch) => {
        dispatch({type: SET_LANGUAGE, payload: data});
    };
};

