// import LocalizedStrings from 'react-native-localization'
import * as RNLocalize from 'react-native-localize';

import Constants from '@common/Constants';

/* API
 setLanguage(languageCode) - to force manually a particular language
 getLanguage() - to get the current displayed language
 getInterfaceLanguage() - to get the current device interface language
 formatString() - to format the passed string replacing its placeholders with the other arguments strings
 */
// import Expo from '@expo';
// const locate = Expo.Util.getCurrentLocaleAsync();

const Languages = {

    en: {
        //Root (Home)
        home: 'Home',
        readlater: 'Read Later',
        category: 'Categories',
        back: ' Back',
        textFilter: 'Recent',

        //Chat
        botTyping: 'Typing...',
        botInput: ' Message... ',

        //Game:
        askText: 'Загадай ворос и нажми на кнопку....',
        askYes: 'Да!',
        askNo: 'Нет!',
        askButton: 'Узнать ответ',
        askAnswerLoading: '...',

        //Login Form
        passwordUp: 'PASSWORD',
        passwordnor: 'password',
        forgotPassword: 'Forget password',
        for: 'Forgot Password',
        login: 'Sign In',
        loginSuccess: "Sign In Successfull",
        noAccount: 'Do not have an account?',
        signup: 'Sign Up',
        signupSuccess: "Sign Up Successfull",

        // MenuSide
        news: 'News',
        contact: 'Contact',
        aboutus: 'About Us',
        setting: 'Setting',
        search: 'Search',
        logout: "Logout",
        bot: "Guru Meditation",

        // Post
        post: 'Post',
        posts: 'Posts',
        feature: 'Feature articles',
        days: 'days',
        editorchoice: 'Editor Choice',
        loading: 'Loading...',
        more: 'More',


        // PostDetail
        comment: 'Comment',
        yourcomment: 'Your Comment',
        relatedPost: 'Related Post',

        all: 'All',
        forLife: 'for lifestyle people',
        powerBy: 'Power by Carnival',
        video: 'Practice',
        fontSize: 'Content font size',
        email: 'EMAIL',
        enterEmail: 'Enter your email',
        enterPassword: 'Type your password',
        photo: 'Photo',
        clear: 'Clear All',
        by: "by",
        name: 'NAME',
        enterName: 'Enter name',
        send: 'Send',
        commentSubmit: 'Your Comment is sent and waiting for approving',
        recent: 'Recent Posts',

        //Layout
        cardView: 'Card ',
        simpleView: 'List View',
        twoColumnView: 'Two Column ',
        threeColumnView: 'Three Column ',
        listView: 'List View',
        default: 'Default',
        advanceView: 'Advance ',
        mansoryView: 'Mansory View',
        horizontalView: 'Horizontal View',

        //readlater
        textBookMark: 'Bookmarks',
        textPosts: 'Posts',
        noBookmark: 'There is no bookmark item',
        ago: 'ago',
        allCategory: 'All Category',
        noResults: 'No Results',

        allTag: 'All Tags',
        user: 'User',
        addComment: 'Add Comment',
        next: 'Next',
        cancel: 'Cancel',
        openInSafari: 'Open in browser',
        sharing: 'Sharing',
        saveToWishlist: 'Save to Wishlist',
        copyLink: 'Copy Link',
        loginRequired: 'Login Required',
        loginRequiredMsg: 'You need to login to write comment.',
        ok: 'Ok',
        add: 'Add',
        errorMsgConnectServer: "Can not connect to server.",
        successMsgPostNews: "You submitted the post successfully.",
        readyToSubmit: 'Ready to submit?',
        publish: "Publish",
        selectTheImage: "Select the image",
        postHeading: 'Post heading',
        selectCategory: 'Select Category',
        allowAccessCameraroll: 'You need to turn on to allow access camera roll',
        submit: "Submit",
        composeTheContent: "Compose the content...",
        notYet: 'Not yet',
        successfull: 'Successfull',
        close: "Close",

        introSkip: 'Skip',
        introNext: 'Next',
        introDone: 'Done',
    },

    ru: {
        //Root (Home)
        home: 'Home',
        readlater: 'Read Later',
        category: 'Энергетические центры',
        back: ' Back',
        textFilter: 'Recent',

        //Chat
        botTyping: 'Печатает...',
        botInput: ' Сообщение... ',

        //Game:
        askText: 'Задай ворос и нажми на кнопку....',
        askYes: 'Да',
        askNo: 'Нет',
        askButton: 'Узнай Да/Нет',
        askAnswerLoading: '...',

        //Login Form
        passwordUp: 'PASSWORD',
        passwordnor: 'password',
        forgotPassword: 'Forget password',
        for: 'Forgot Password',
        login: 'Sign In',
        loginSuccess: "Sign In Successfull",
        noAccount: 'Do not have an account?',
        signup: 'Sign Up',
        signupSuccess: "Sign Up Successfull",

        // MenuSide
        news: 'Записи',
        contact: 'Contact',
        aboutus: 'Обо мне',
        setting: 'Setting',
        search: 'Поиск',
        logout: "Logout",
        bot: "Гуру-Медитации",

        // Post
        post: 'Post',
        posts: 'Posts',
        feature: 'Feature articles',
        days: 'days',
        editorchoice: 'Editor Choice',
        loading: 'Загрузка...',
        more: 'Еще',

        // PostDetail
        comment: 'Comment',
        yourcomment: 'Your Comment',
        relatedPost: 'Похожие записи',

        all: 'All',
        forLife: 'for lifestyle people',
        powerBy: 'Power by Carnival',
        video: 'Аффирмации',
        fontSize: 'Content font size',
        email: 'EMAIL',
        enterEmail: 'Enter your email',
        enterPassword: 'Type your password',
        photo: 'Photo',
        clear: 'Clear All',
        by: "by",
        name: 'NAME',
        enterName: 'Enter name',
        send: 'Send',
        commentSubmit: 'Your Comment is sent and waiting for approving',
        recent: 'Recent Posts',

        //Layout
        cardView: 'Card ',
        simpleView: 'List View',
        twoColumnView: 'Two Column ',
        threeColumnView: 'Three Column ',
        listView: 'List View',
        default: 'Default',
        advanceView: 'Advance ',
        mansoryView: 'Mansory View',
        horizontalView: 'Horizontal View',

        //readlater
        textBookMark: 'Избранное',
        textPosts: 'Posts',
        noBookmark: 'Список пуст!',
        ago: 'ago',
        allCategory: 'All Category',
        noResults: 'поиск пуст',

        allTag: 'All Tags',
        user: 'User',
        addComment: 'Add Comment',
        next: 'Next',
        cancel: 'Отменить',
        openInSafari: 'Отрыть в браузере',
        sharing: 'Поделиться',
        saveToWishlist: 'Save to Wishlist',
        copyLink: 'Копировать ссылку',
        loginRequired: 'Login Required',
        loginRequiredMsg: 'You need to login to write comment.',
        ok: 'Ok',
        add: 'Add',
        errorMsgConnectServer: "Can not connect to server.",
        successMsgPostNews: "You submitted the post successfully.",
        readyToSubmit: 'Ready to submit?',
        publish: "Publish",
        selectTheImage: "Select the image",
        postHeading: 'Post heading',
        selectCategory: 'Select Category',
        allowAccessCameraroll: 'You need to turn on to allow access camera roll',
        submit: "Submit",
        composeTheContent: "Compose the content...",
        notYet: 'Not yet',
        successfull: 'Successfull',
        close: "Close",

        introSkip: 'Пропустить',
        introNext: 'Далее',
        introDone: 'Закрыть',
    },

    uk: {},
    ///Put other languages here
};


//const fallback = { languageTag: 'en', isRTL: false };
const {
    languageTag,
    isRTL
} = RNLocalize.findBestAvailableLanguage(Constants.defaultLanguages) || Constants.fallbackLanguage;
//console.log('languageTag L:'+languageTag);
// let Language = Languages[locate];
// if (Language == null)	{
const Language = Languages[languageTag];
// }


export default Language

