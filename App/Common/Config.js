/** @format */

import Constants from './Constants';
import Language from "./Languages";

export default {
    /**
     * The detail document from: https://beonews.inspireui.com
     * Step 1: Moved to AppConfig.json
     */
    // IsRequiredLogin:true,
    Banner: {
        visible: false,
        sticky: true,
        tag: [],
        categories: [],
    },

    /**
     * Advance config
     * CategoryVideo: config the category id for video page
     *
     * */
    // Category video id from the menu
    //CategoryVideo: 84,
    CategoryVideoMulti: {
        ru: [17],
        en: [179],
    },

    PostDetailCustomFieldId: 'mp3',

    imageCategories: {
        //  bioenergetika: require('@images/category/cat0.png'),
        // blog: require('@images/category/cat4.png'),
        // meditatsiya_dnia:require('@images/category/cat2.png'),
        // meditatsiya_dvegenie:require('@images/category/cat8.png'),
        // meditatsiya:require('@images/category/cat1.png'),
        // meditatsiya_osoznannosti:require('@images/category/cat5.png'),
        // staticheskaya_meditatsiya:require('@images/category/cat6.png'),
        // pozitivnaya_psihologiya:require('@images/category/cat9.png'),
        // son:require('@images/category/cat3.png'),
        // tsigun:require('@images/category/cat7.png'),


        // bioenergetika: require('@images/category/lock.png'),
        // bioenergy: require('@images/category/lock.png'),
        // mental_health: require('@images/category/hand.png'),
        // mentalnoe_zdorove: require('@images/category/hand.png'),
        // meditation: require('@images/category/guy.png'),
        // meditatsiya: require('@images/category/guy.png'),

        // chakra1: require('@images/category/chakra1.png'),
        // chakra2: require('@images/category/chakra2.png'),
        // chakra3: require('@images/category/chakra3.png'),
        // chakra4: require('@images/category/chakra4.png'),
        // chakra5: require('@images/category/chakra5.png'),
        // chakra6: require('@images/category/chakra6.png'),
        // chakra7: require('@images/category/chakra7.png'),


        // uncategorized_en: require('@images/category/uncategorized.png'), ///
        // uncategorized_ru: require('@images/category/uncategorized.png'), ///
        // uncategorized_uk: require('@images/category/uncategorized.png'), ///
        //
        // bioenergetika: require('@images/category/photo.png'), ///
        // bioenergy: require('@images/category/photo.png'), ///
        //
        // blagosostoyanie: require('@images/category/money.png'),
        // wealth: require('@images/category/money.png'),
        // meditatsiya: require('@images/category/video.png'),
        // mindfulness_meditation: require('@images/category/video.png'),
        // ozdorovitelnaya_praktika: require('@images/category/guy.png'),
        // mind_body_spirit_practice: require('@images/category/guy.png'),
        // pozitivnaya_psihologiya: require('@images/category/hand.png'),
        // positive_psychology: require('@images/category/hand.png'),
        // lyubov_k_sebe: require('@images/category/lock.png'),
        // self_love: require('@images/category/lock.png'),
        // son: require('@images/category/melody.png'),
        // meditation_for_sleep: require('@images/category/melody.png'),
        // rasslablenie: require('@images/category/bell.png'),
        // relaxation: require('@images/category/bell.png')

    },

    // Custom page from left menu side
    // CustomPages: {
    //     contact_id: 11,
    //     aboutus_id: 8513,
    // },
    CustomPagesMulti: {
        en: {
            contact_id: 11,
            aboutus_id: 474,
        },
        ru: {
            contact_id: 11,
            aboutus_id: 474,
        }
    },

    // config for Firebase, use to sync user data across device and favorite post
    Firebase: {
        // apiKey: 'AIzaSyDnBpxFOfeG6P06nK97hMg01kEgX48JhLE',
        // authDomain: 'beonews-ef22f.firebaseapp.com',
        // databaseURL: 'https://beonews-ef22f.firebaseio.com',
        // storageBucket: 'beonews-ef22f.appspot.com',
        // messagingSenderId: '1008301626030',
        // readlaterTable: 'list_readlater',
    },

    // config for log in by Facebook
    Facebook: {
        showAds: false,
        adPlacementID: '',
        logInID: '',
        sizeAds: 'standard', // standard, large
    },

    // config for log in by Google
    // Google: {
    //   analyticId: 'UA-90561349-1',
    //   androidClientId:
    //     '338838704385-1om86241pq2qpg4qi677jb1ndo5jqfh2.apps.googleusercontent.com',
    //   iosClientId:
    //     '338838704385-1om86241pq2qpg4qi677jb1ndo5jqfh2.apps.googleusercontent.com',
    // },

    // The advance layout
    AdvanceLayout: [
        Constants.Layout.threeColumn,
        Constants.Layout.threeColumn,
        Constants.Layout.threeColumn,
        Constants.Layout.list,
        Constants.Layout.list,
        Constants.Layout.list,
        Constants.Layout.list,
        Constants.Layout.card,
        Constants.Layout.column,
        Constants.Layout.column,
    ],

    // config for log in by Admob
    AdMob: {
        visible: false,
        deviceID: '',
        unitID: '',
        unitInterstitial: '',
        isShowInterstital: false,
    },

    // tab animate
    tabBarAnimate: Constants.Animate.zoomIn,

    // config default for left menu
    LeftMenuStyle: Constants.LeftMenu.scale,

    notification: {
        // AppId: '85cbc2b5-4e0d-4214-9653-8054d06f4256',
        // NewAppId: '88b0e176-5756-47b7-b061-aacea262421d',
    },

    OneSignal: {
        appId: '1ac05529-0094-4eda-8dbd-ba6046812beb',
    },

    // update 18 May
    // showLayoutButton: Ability to show/hide the switch layout button at homescreen
    // homeLayout: default homeLayout UI: Constants.Layout.mansory, Constants.Layout.horizontal
    // showSwitchCategory: show the switch button on categories page
    showLayoutButton: false,
    homeLayout: Constants.Layout.horizontal,
    showSwitchCategory: false,
    EnableSanitizeHtml: true,
    RequiredLogin: false,
    showSubCategoryScreen: false,

    showAppIntro: true,
    intro: [
        // {
        //   title: 'Welcome to BeoFoods',
        //   description:
        //     'Get the latest food receipt on your hand and sharing with your friends',
        //   backgroundColor: '#D5E8ED',
        //   source: require('@images/lottie/loading.json'),
        //   button: 'GET STARTED',
        // },
        {
            title: 'Как найти подлинное счастье? ',
            description: '',
            backgroundColor: '#FFF',
            source: require('@images/lottie/intro-1'),
            button: Language.introNext,
        },
        // {
        //     title: 'Техники медитации и духовных упражнений на чакры, чтобы исцелить себя',
        //     description: '',
        //     backgroundColor: '#FFF',
        //     source: require('@images/lottie/intro-2'),
        //     button: Language.introNext,
        // },
        // {
        //     title: 'Аффирмации для чакр на принятие себя и доверие миру',
        //     description: '',
        //     backgroundColor: '#FFF',
        //     source: require('@images/lottie/intro-3'),
        //     button: Language.introNext,
        // },
        {
            title: 'Медитация помогает нам понять, что подлинное счастье заключено в нас самих.',
            description: '',
            backgroundColor: '#D6D7DD',
            source: require('@images/lottie/intro-2'),
            button: Language.introDone,
        },
    ],
};
