import { BlurView, VibrancyView } from "@react-native-community/blur";
import {Platform, View} from 'react-native';
import React, {Component} from "react";

export default class Blur extends Component {
  render() {
    const {tint, style, intensity, children} = this.props;

    if (Platform.OS == 'android') {
      return <View style={style}>{children}</View>;
    }

    return <BlurView blurType={tint}
                     style={style}
                     blurAmount={intensity}>
      {children}
    </BlurView>
  }
}