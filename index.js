/** @format */

import React from "react";

import { AppRegistry } from "react-native";
import CosmosMe from "./App";
import 'react-native-gesture-handler'
import TrackPlayer from 'react-native-track-player';


AppRegistry.registerComponent('CosmosMe', () => CosmosMe)

TrackPlayer.registerPlaybackService(() => require('./service.js'));
