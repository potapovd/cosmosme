/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.deo.wp.rn.cosmosme;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.deo.wp.rn.cosmosme";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
}
